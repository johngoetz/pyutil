r"""`appdirs` is guaranteed to have the following methods for Darwin, Linux and
Windows. `app` is the application name, `org` is the organization name and `qual` is
the organization qualifier (like "com" or "org").

    appdirs.cache_dir(app, org, qual)
    appdirs.config_dir(app, org, qual)
    appdirs.data_dir(app, org, qual)

The `qual` parameter is ignored on Linux and Windows and the `org` parameter is ignored
on Linux. For all platforms, both `org` and `qual` may be omitted. Additionally, Linux
will have:

    appdirs.runtime_dir(app)

And Windows will have:

    appdirs.local_data_dir(app, org)
    appdirs.remote_data_dir(app, org)

The config directory will look like this for the three platforms:

    Windows: C:\Users\user\AppData\Roaming\Config\org\App
    Linux:   /home/user/.config/App
    Darwin:  /Users/user/Library/Preferences/qual.org.App

The data directory will look like this for the three platforms:

    Windows: C:\Users\user\AppData\Local\org\App
    Linux:   /home/user/.local/share/App
    Darwin:  /Users/user/Library/Appplication Support/qual.org.App
"""
import os
import pathlib
import platform


def environ_path(key):
    value = os.environ.get(key, None)
    if value:
        return pathlib.Path(value)


class xdg:
    @staticmethod
    def cache_dir():
        return environ_path('XDG_CACHE_HOME')

    @staticmethod
    def config_dir():
        return environ_path('XDG_CONFIG_HOME')

    @staticmethod
    def data_dir():
        return environ_path('XDG_DATA_HOME')

    @staticmethod
    def runtime_dir():
        return environ_path('XDG_RUNTIME_DIR')


class User:
    @staticmethod
    def home_dir():
        return pathlib.Path.home()


class LinuxUser(User):
    @staticmethod
    def cache_dir():
        cached = xdg.cache_dir()
        return cached or User.home_dir() / '.cache'

    @staticmethod
    def config_dir():
        confd = xdg.config_dir()
        return confd or User.home_dir() / '.config'

    @staticmethod
    def data_dir():
        datad = xdg.data_dir()
        return datad or User.home_dir() / '.local/share'

    @staticmethod
    def runtime_dir():
        rtd = xdg.runtime_dir()
        return rtd or pathlib.Path(f'/run/user/{os.getuid()}')


class LinuxApp:
    @staticmethod
    def _app_path(app):
        return app.replace(' ', '').replace('.', '')

    @staticmethod
    def cache_dir(app, org=None, qual=None):
        return LinuxUser.cache_dir() / LinuxApp._app_path(app)

    @staticmethod
    def config_dir(app, org=None, qual=None):
        return LinuxUser.config_dir() / LinuxApp._app_path(app)

    @staticmethod
    def data_dir(app, org=None, qual=None):
        return LinuxUser.data_dir() / LinuxApp._app_path(app)

    @staticmethod
    def local_data_dir(app, org=None):
        return LinuxApp.data_dir(app, org)

    @staticmethod
    def roaming_data_dir(app, org=None):
        return LinuxApp.data_dir(app, org)

    @staticmethod
    def runtime_dir(app):
        return LinuxUser.runtime_dir() / LinuxApp._app_path(app)


class DarwinUser(User):
    @staticmethod
    def cache_dir():
        cached = xdg.cache_dir()
        return cached or User.home_dir() / 'Library/Caches'

    @staticmethod
    def config_dir():
        confd = xdg.config_dir()
        return confd or User.home_dir() / 'Library/Preferences'

    @staticmethod
    def data_dir():
        datad = xdg.data_dir()
        homed = User.home_dir()
        return datad or homed / 'Library/Application Support'


class DarwinApp:
    @staticmethod
    def _app_path(app, org, qual):
        norm = lambda name: name.replace(' ', '-').replace('.', '')
        if qual and org:
            return '.'.join([qual, norm(org), norm(app)])
        elif org:
            return '.'.join([norm(org), norm(app)])
        else:
            return norm(app)

    @staticmethod
    def cache_dir(app, org=None, qual=None):
        return DarwinUser.cache_dir() / DarwinApp._app_path(app, org, qual)

    @staticmethod
    def config_dir(app, org=None, qual=None):
        return DarwinUser.config_dir() / DarwinApp._app_path(app, org, qual)

    @staticmethod
    def data_dir(app, org=None, qual=None):
        return DarwinUser.data_dir() / DarwinApp._app_path(app, org, qual)

    @staticmethod
    def local_data_dir(app, org=None):
        return DarwinApp.data_dir(app, org)

    @staticmethod
    def roaming_data_dir(app, org=None):
        return DarwinApp.data_dir(app, org)

    @staticmethod
    def runtime_dir(app):
        return DarwinApp.cache_dir(app)


class WindowsUser(User):
    @staticmethod
    def home_dir():
        homed = environ_path('USERPROFILE')
        return homed or User.home_dir()

    @staticmethod
    def local_data_dir():
        datad = environ_path('LOCALAPPDATA')
        return datad or WindowsUser.home_dir() / 'AppData/Local'

    @staticmethod
    def roaming_data_dir():
        datad = environ_path('APPDATA')
        return datad or WindowsUser.home_dir() / 'AppData/Roaming'


class WindowsApp:
    @staticmethod
    def _app_path(app, org):
        return pathlib.Path(org, app) if org else app

    @staticmethod
    def cache_dir(app, org=None, qual=None):
        return WindowsUser.local_data_dir() / WindowsApp._app_path(app, org) / 'Cache'

    @staticmethod
    def config_dir(app, org=None, qual=None):
        return (
            WindowsUser.roaming_data_dir() / WindowsApp._app_path(app, org) / 'Config'
        )

    @staticmethod
    def local_data_dir(app, org=None):
        return WindowsUser.local_data_dir() / WindowsApp._app_path(app, org) / 'Data'

    @staticmethod
    def roaming_data_dir(app, org=None):
        return WindowsUser.roaming_data_dir() / WindowsApp._app_path(app, org) / 'Data'

    @staticmethod
    def data_dir(app, org=None, qual=None):
        return WindowsApp.local_data_dir(app, org)

    @staticmethod
    def runtime_dir(app):
        return WindowsApp.cache_dir(app)


appdirs = {
    'Darwin': DarwinApp,
    'Linux': LinuxApp,
    'Windows': WindowsApp,
}.get(platform.system(), 'unknown platform')
