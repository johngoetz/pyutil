import asyncio
import collections
import contextlib
import os
import pathlib
import shlex
import subprocess


async def awaitable(obj):
    """Force obj to be awaitable."""
    if isinstance(obj, collections.abc.Awaitable):
        return await obj
    else:
        return obj


async def pipe_stream(proc, istream, callback, limit):
    """Read streaming data asynchronously sending it to a callback.

    Parameters:
        proc (asyncio.subprocess.Process): The subprocess that is attached to
            the stream.
        istream (asyncio.StreamReader): The input stream awaitable object.
        callback (collections.abc.Callable): Function with arguments (proc,
            data) where data has been read from the istream.
        limit (int): Stream limit in bytes. This must be set upon creation of
            the asynchronous subprocess object and is used here to obtain line
            data in chunks.
    """
    while proc.returncode is None:
        try:
            data = await istream.readuntil(b'\n')
        except asyncio.LimitOverrunError:
            data = await istream.read(limit)
        except asyncio.IncompleteReadError as e:
            callback(proc, e.partial)
            break
        if data is None:
            break
        callback(proc, data)


def async_poller(fn, interval):
    """Returns an asynchronous function that calls fn(proc) on some interval.

    Parameters:
        fn (callable): This is a function taking a single
            argument proc which will be a asyncio.subprocess.Process instance.
        interval (float): Seconds to sleep between calling the function.

    The function will only be called if the subprocess is running just before
    though it is still not guaranteed to be running upon entering of the
    function.
    """

    async def poll(proc):
        while proc.returncode is None:
            await fn(proc)
            await asyncio.sleep(interval)

    return poll


def decode_output(text, strip_text):
    """Transform data as text and optionally strip trailing spaces.

    text (bool): decode data bytes as text
    strip_text (bool): strip trailing spaces (only if text is True)
    """
    if text:
        if strip_text:
            return lambda data: data.decode().rstrip()
        else:
            return lambda data: data.decode()
    else:
        return lambda data: data


def write_to(ostream, open_streams, text, strip_text):
    """Interpret output stream as a filename, callable or writable stream.

    Parameters:
        ostream: This may be None, a callable, a string filename or an open
            stream with write() and flush() methods. If a string, the file will
            be opened and appended to the open_streams parameter.
        open_streams: External list to which open file handles are appended.
            It is the responsibility of the caller to close these.
        text (bool): Decode output bytes as text.
        strip_text (bool): Strip trailing spaces from text (only if text is True).

    Returns:
        tuple:
            * ostream parameter to be passed to stdout/stderr
            * stream callback which will be called on any output to the stream
    """
    if ostream is None:
        return None, None

    decode = decode_output(text, strip_text)

    if isinstance(ostream, collections.abc.Callable):

        def write_to_stream(proc, line, ostream=ostream, decode=decode):
            ostream(decode(line))

    elif isinstance(ostream, int):
        return ostream, None
    else:
        if isinstance(ostream, (str, pathlib.Path)):
            ostream = open(ostream, ('t' if text else 'b') + 'w')
            open_streams.append(ostream)

        assert hasattr(ostream, 'write') and hasattr(ostream, 'flush')

        def write_to_stream(proc, line, ostream=ostream, decode=decode):
            ostream.write(decode(line))

    return asyncio.subprocess.PIPE, write_to_stream


@contextlib.contextmanager
def open_subprocess(cmd, *args, shell=False, **kwargs):
    """Open a synchronous subprocess instance."""
    cmd = [cmd] + list(args)
    if shell:
        cmd = shlex.join(cmd)
    proc = subprocess.Popen(cmd, shell=shell, **kwargs)
    try:
        yield proc
    finally:
        if proc.returncode is None:
            proc.terminate()
            proc.wait()


@contextlib.asynccontextmanager
async def aopen_subprocess(
    cmd,
    *args,
    stdout=None,
    stderr=None,
    limit=2**16,
    text=True,
    strip_text=False,
    poll=None,
    poll_interval=1,
    **kwargs
):
    """Open an async subprocess instance.

    Parameters:
        cmd (str): The command to execute.
        *args (str): Arguments to pass to the command.
        stdout: This and **stderr** accept any of the following values:

            * A value of `None` passes the output directly to the console.
            * An object instance that has two methods: ``write(line)`` and
              ``flush()`` where ``line`` is a line of output.
            * A function object taking two parameters: ``(proc, line)`` where
              ``line`` is a line of output.
            * A string filename which will be opened and streamed to.
            * Any other value that the **stdout** parameter to
              ``asyncio.create_subprocess_exec()`` accepts.

        stderr: This takes the same values as **stdout**.
        env (dict): Environment variables set in the process.

        stdout options:
            subprocess.PIPE
            sys.stdout
            asyncio.subprocess.PIPE
            str/pathlib.Path (filename)
            file-like object (fileno(), write(data), flush())
            writable object (write(data), flush())
            callable (callback(data))
    """
    if all(p is None for p in (stdout, stderr, poll)):
        # no pipes or polling so no need for async operations
        yield open_subprocess(cmd, *args, **kwargs)
    else:
        open_streams = []
        try:
            outpipe, stdout = write_to(stdout, open_streams, text, strip_text)
            errpipe, stderr = write_to(stderr, open_streams, text, strip_text)

            if kwargs.pop('shell', False):
                async_create_subprocess = asyncio.create_subprocess_shell
                cmd = shlex.join([cmd] + list(args))
                args = tuple()
            else:
                async_create_subprocess = asyncio.create_subprocess_exec

            proc = await async_create_subprocess(
                cmd, *args, stdout=outpipe, stderr=errpipe, limit=limit, **kwargs
            )

            if stdout:
                pipe_stdout = pipe_stream(proc, proc.stdout, stdout, limit)
                asyncio.create_task(pipe_stdout)
            if stderr:
                pipe_stderr = pipe_stream(proc, proc.stderr, stderr, limit)
                asyncio.create_task(pipe_stderr)
            if poll:
                asyncio.create_task(async_poller(poll, poll_interval)(proc))

            try:
                yield proc
            finally:
                if proc.returncode is None:
                    proc.terminate()
                    await awaitable(proc.wait())
        finally:
            for stream in open_streams:
                stream.close()


def async_context(fn, timeout, cmd):
    """Returns an asynchronous function that awaits context(proc).

    The process will be terminated upon exit of the context() function.

    Parameters:
        fn (async callable): This is a function taking a single argument
            proc which will be a asyncio.subprocess.Process instance.
        timeout (float): Seconds before terminating the subprocess. If set,
            this method will raise subprocess.TimeoutExpired on timeout.
        cmd (str): String representation of the command that is running. This
            is used when a timeout exception is raised.
    """

    async def callback(proc, timeout=timeout, cmd=cmd):
        raised_error = None
        try:
            await asyncio.wait_for(fn(proc), timeout)
        except asyncio.TimeoutError as e:
            raised_error = e
        finally:
            if proc.returncode is None:
                try:
                    proc.terminate()
                except ProcessLookupError:
                    if proc.returncode is None:
                        raise
        if isinstance(raised_error, asyncio.TimeoutError):
            raise subprocess.TimeoutExpired(cmd, timeout)

    return callback


def run_subprocess(cmd, *args, context=None, timeout=None, **kwargs):
    """Run an asynchronous asyncio.subprocess.Process instance.

    Parameters:
        cmd (str): The command to run.
        *args (str): Arguments passed to command.
        context (async callable): This is a function taking a single argument
            proc which will be a asyncio.subprocess.Process instance.
        timeout (float): Seconds before terminating the subprocess. If set,
            this method will raise subprocess.TimeoutExpired on timeout.
        **kwargs: Passed to open_subprocess().
    """

    async def run_proc(cmd, *args, context=context, timeout=timeout, **kwargs):
        async with aopen_subprocess(cmd, *args, **kwargs) as proc:
            if context or timeout:
                if not context:
                    context = lambda proc: proc.wait()
                await async_context(context, timeout, [cmd] + list(args))(proc)
            return await awaitable(proc.wait())

    return asyncio.run(run_proc(cmd, *args, **kwargs))
