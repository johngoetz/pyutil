import contextlib
import importlib
import pathlib
import sys


def import_from_file(file_path, module_name=None):
    file_path = pathlib.Path(file_path).expanduser()
    assert file_path.is_file(), f'{file_path} is not a file.'
    if not module_name:
        module_name = file_path.stem.replace('-', '_')
    loader = importlib.machinery.SourceFileLoader(module_name, str(file_path))
    spec = importlib.util.spec_from_loader(module_name, loader)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module


@contextlib.contextmanager
def syspath(directory):
    sys.path.insert(1, directory)
    try:
        yield
    finally:
        if directory in sys.path:
            sys.path.remove(directory)


@contextlib.contextmanager
def temporary_import(module, directory=None):
    ctx = syspath(directory) if directory else contextlib.nullcontext()
    with ctx:
        try:
            yield importlib.import_module(module)
        finally:
            if module in sys.modules:
                del sys.modules[module]
