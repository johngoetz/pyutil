import argparse
import configparser
import copy
import logging
import os
import pathlib
import sys

from gettext import gettext as _

import toml

from .appdirs import appdirs
from .cmake import as_bool

log = logging.getLogger(__name__)


# patch: argparse.ArgumentParser._check_value(self, action, value)
# Show only the full command names instead of all aliases when
# passing in an invalid subcommand.
def argparse_ArgumentParser_check_value(self, action, value):
    # converted value must be one of the choices (if specified)
    if action.choices is not None and value not in action.choices:
        choices = getattr(action, 'commands', action.choices)
        choices = ', '.join(map(repr, choices))
        msg = _(f'invalid choice: {value!r} (choose from {choices})')
        raise argparse.ArgumentError(action, msg)


argparse.ArgumentParser._check_value = argparse_ArgumentParser_check_value


class CommandFormatter(
    argparse.ArgumentDefaultsHelpFormatter,
    argparse.RawDescriptionHelpFormatter,
):
    """Default formatter for subcommands"""


class SubcommandParser:
    def __init__(self, command, envvar, *args, func=None, **kwargs):
        self.command = command
        self.envvar = f'{envvar}_{self.command.upper().replace("-", "_")}'
        self._init_args = args
        self.func = func or self.print_help
        self._init_kwargs = kwargs
        self.command_args = argparse.ArgumentParser(add_help=False)
        self.args = self.command_args.add_argument_group(f'options for {self.command}')
        self.commands = {}

    def _aliases(self, cmds):
        aliases = []
        for i in range(1, len(self.command)):
            alias = self.command[:i]
            if len(list(filter(lambda c: c.startswith(alias), cmds))) == 1:
                aliases.append(alias)
        return aliases

    def _build_parser(self, subparsers, parents, pos, cmds):
        parents = parents + [self.command_args]
        self.parser = subparsers.add_parser(
            self.command,
            *self._init_args,
            aliases=self._aliases(cmds),
            parents=parents,
            **self._init_kwargs,
        )
        if 'help' in self._init_kwargs:
            # remove aliases from metavar when help string is specified
            subparsers._choices_actions[-1].metavar = self.command
        self.parser.set_defaults(func=self.func)
        self._build_command_parsers(parents, pos)

    def add_argument(self, *args, **kwargs):
        if __debug__:
            if self.commands and args[0][0] not in self.args.prefix_chars:
                msg = 'positional arguments and subcommands are mutually exclusive'
                raise ValueError(msg)
        arg = self.args.add_argument(*args, **kwargs)
        if arg.help:
            arg.help += f' (env: {self._envvar(arg)})'
        return arg

    def add_command(self, command, *args, func=None, **kwargs):
        if __debug__:
            if any (len(action.option_strings) == 0 for action in self.args._actions):
                msg = 'positional arguments and subcommands are mutually exclusive'
                raise ValueError(msg)
        kw = dict(
            func=func,
            formatter_class=self._init_kwargs.get('formatter_class', CommandFormatter),
        )
        kw.update(**kwargs)
        self.commands[command] = SubcommandParser(command, self.envvar, *args, **kw)
        return self.commands[command]

    def print_help(self, *_):
        self.parser.print_help()

    def _add_subparsers(self, pos=0):
        cmds = sorted(self.commands.keys())
        subparsers = self.parser.add_subparsers(
            dest=f'_cmd{pos}',
            metavar=f'{{{",".join(cmds)}}}' if len(cmds) < 4 else 'CMD',
            help=f'command to run: {", ".join(cmds)}',
        )
        subparsers.commands = cmds
        return cmds, subparsers

    def _build_command_parsers(self, parents, pos=0):
        if self.commands:
            cmds, subparsers = self._add_subparsers(pos)
            for command, command_parser in self.commands.items():
                command_parser._build_parser(subparsers, parents, pos + 1, cmds)

    def _envvar(self, arg):
        return f'{self.envvar}_{arg.dest.upper().replace("-", "_")}'

    def _set_defaults(self, config):
        for arg in self.args._actions:
            envvar = self._envvar(arg)
            if envvar in os.environ:
                default = os.environ[envvar]
                arg.default = arg.type(default) if arg.type else default
            elif arg.dest in config:
                arg.default = config[arg.dest]
        for cmd, cmdparser in self.commands.items():
            cmdparser._set_defaults(config.get(cmd, {}))


class CommandParser(SubcommandParser):
    """Sub-command command-line argument parser

    Example usage::

        def init(args):
            print(args)

        def push(args):
            print(args)

        parser = pyutil.CommandParser('program')
        parser.add_argument('-o', '--opt', help='global option')

        initparser = parser.add_command('init', func=init, help='init')
        initparser.add_argument('input', help='init input')

        pushparser = parser.add_command('push', func=push, help='push')
        pushparser.add_argument('-o', '--output', help='push output')

    Values for arguments follow these rules:

    command line > environment > config file > defaults

    Defaults are overridden by config file which are overridden by
    environment variables::

        global-key = value
        [command]
        command-key = value
        [command.subcommand]
        subcommand-key = value

    all environment variables that look like this will be honored::

        PROGRAM_KEY=value
        PROGRAM_COMMAND_KEY=value
        PROGRAM_COMMAND_SUBCOMMAND_KEY=value
    """

    def __init__(self, prog, *args, formatter_class=CommandFormatter, **kwargs):
        self._init_args = args
        self._init_kwargs = kwargs
        self._init_kwargs.update(formatter_class=formatter_class)
        self.default_prefix = kwargs.get('prefix_chars', '-')[0]

        self.args = argparse.ArgumentParser(
            add_help=False, prog=prog
        )
        self.prog_name = pathlib.Path(self.args.prog).stem

        self.envvar = self.prog_name.upper().replace('-', '_')
        self.commands = {}
        self.config_file_parser = None

        help_parser = self.add_command('help', func=self._help_cmd)
        help_parser.add_argument('commands', nargs='*')

    def _build_parser(self, args, namespace):
        self._update_defaults(args, namespace)
        parents = []
        if self.config_file_parser:
            parents.append(self.config_file_parser)
        parents.append(self.args)
        self.parser = argparse.ArgumentParser(
            *self._init_args, prog=self.prog_name, parents=parents, **self._init_kwargs
        )
        self.parser.set_defaults(func=self.print_help)
        self._build_command_parsers(parents)

    def _help_cmd(self, args):
        self.parse_args(args.commands + [(self.default_prefix * 2) + 'help'])

    def _update_defaults(self, args, namespace):
        config = {}
        if self.config_file_parser:
            args, _ = self.config_file_parser.parse_known_args(args, namespace)
            if args.config_file.is_file():
                try:
                    config = toml.load(args.config_file)
                except toml.decoder.TomlDecodeError as e:
                    log.error(f'ERROR: Config file: {args.config_file}')
                    log.error(f'ERROR: {e}')
                    sys.exit(1)
        self._set_defaults(config)

    def _parse_commands(self, args):
        args, _ = self.parser.parse_known_args(args)
        args_dict = vars(args)
        cmd_keys = sorted(filter(lambda k: k.startswith('_cmd'), args_dict))
        return list(filter(None, (args_dict[k] for k in cmd_keys)))

    def parse_args(self, args=None, namespace=None):
        if not hasattr(self, 'parser'):
            self._build_parser(args, namespace)
        args = sys.argv[1:] if args is None else args
        if commands := self._parse_commands(args):
            args = args.copy()
            for command in commands:
                args.remove(command)
            args = commands + args
        args = self.parser.parse_args(args=args, namespace=namespace)
        if all(key in args for key in ('verbose', 'quiet', 'verbosity')):
            if args.verbosity is None:
                args.verbosity = int(args.verbose) - int(args.quiet)
            args.verbosity = min(max(int(args.verbosity), -3), 3)
        return args

    def add_verbosity_arguments(self):
        self.add_argument(
            (self.default_prefix * 2) + 'verbose',
            self.default_prefix + 'v',
            action='count',
            default=0,
            help="""Increase verbosity, may be repeated, competes with --quiet.
                    Verbosity is calculated as (verbose - quiet) and can be in
                    the range [-3, 3]. Can be overridden with --verbosity.""",
        )
        self.add_argument(
            (self.default_prefix * 2) + 'quiet',
            self.default_prefix + 'q',
            action='count',
            default=0,
            help="""Decrease verbosity, may be repeated, competes with --verbose.
                    Verbosity is calculated as (verbose - quiet) and can be in
                    the range [-3, 3]. Can be overridden with --verbosity.""",
        )
        self.add_argument(
            (self.default_prefix * 2) + 'verbosity',
            default=None,
            help="""Override any --verbose or --quiet options and set the verbosity
                    directly. Must be an integer in the range [-3, 3].""",
        )

    def add_version_argument(self, version):
        self.add_argument(
            (self.default_prefix * 2) + 'version',
            action='version',
            version=version,
        )

    def add_config_file_argument(self):
        envvar = f'{self.envvar}_CONFIG_FILE'
        default = os.environ.get(
            envvar, appdirs.config_dir(self.prog_name) / 'config.toml'
        )
        self.config_file_parser = argparse.ArgumentParser(add_help=False)
        self.config_file_parser.add_argument(
            (self.default_prefix * 2) + 'config-file',
            self.default_prefix + 'C',
            default=default,
            metavar='FILE',
            type=pathlib.Path,
            help=f"""configuration TOML file with sections of the form
                     "[command.sub-command]". The defaults displayed in this help
                     output reflect the settings loaded from this file. (env:
                     {envvar})""",
        )

    def _zsh_autocomplete(self):
        script = [
            '#!/bin/zsh',
            'typeset -A opt_args',
            'endopt="!(-)--end-of-options"',
        ]

        if any(arg.option_strings for arg in self.args._actions):
            script.append(f'local -s {self.prog_name}_opts=(')
            for arg in self.args._actions:
                if arg.option_strings:
                    script.append(f'  {{{",".join(arg.option_strings)}}}"[{arg.help}]"')
            script.append(')')

        #for cmd, cmdparser in self.commands.items():
        #    script.append(f'    "{cmd}[{cmdparser.help}]"')

        script.extend((
            f'_{self.prog_name}() {{',
            '  local line state',
            f'  local -a {self.prog_name}_commands=(',
        ))
        for cmd, cmdparser in self.commands.items():
            script.append(f'    "{cmd}[]"') #{cmdparser.help}]"')
        script.extend((
            f'  _arguments -C -s ${self.prog_name}_opts "1: :->cmds" "*::arg:->args"',
            '  case "$state" in',
            f'    cmds) _values "{self.prog_name} command" ${self.prog_name}_commands;;',
            '    args)',
            '      case $line[1] in',
        ))
        for cmd, cmdparser in self.commands.items():
            script.append(f'        {cmd}) _{self.prog_name}_{cmd};;')
        script.extend((
            '      esac',
            '    ;;',
            '  esac',
            '}',
        ))

        script.append(f'compdef _{self.prog_name} {self.prog_name}')
        return '\n'.join(script)
