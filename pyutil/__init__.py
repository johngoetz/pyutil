from .version import __version__

import os
import sys

from . import patch_python as _

from colorama import just_fix_windows_console
just_fix_windows_console()

from . import gitlab, kh2reg

from .appdirs import appdirs
from .cmake import as_bool, cmake_bool
from .cmdparser import CommandParser
from .env import (
    append_paths_to_env,
    env,
    prepend_paths_to_env,
    join_paths,
)
from .file_operations import (
    ClosedTemporaryFile,
    directory_size,
    ExistingDir,
    ExistingFile,
    rsync,
    Secret,
    synctree,
    tee,
)
from .import_operations import import_from_file, syspath, temporary_import
from .platform_id import platform_id
from .repository import (
    git_enable_symlinks,
    is_git_repo,
    git_topdir,
    GitRepository,
)
from .virtual_display import run_in_virtual_display, virtual_display
from .working_indicators import (
    DataTransferProgressBar,
    ProgressBall,
    ProgressBar,
    ProgressDots,
    ProgressWave,
)

if sys.version_info >= (3, 7):
    from .subproc import run_subprocess
