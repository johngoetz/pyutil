import filecmp
import logging
import os
import shutil
import stat
import tempfile
import contextlib
import pathlib
import platform
import shlex
import sys
import subprocess

from os import path

from termcolor import colored


log = logging.getLogger()


def directory_size(topdir):
    files = filter(pathlib.Path.is_file, pathlib.Path(topdir).rglob('*'))
    file_sizes = map(lambda p: p.stat(follow_symlinks=False).st_size, files)
    return sum(file_sizes)


def synctree(srcDir, dstDir):
    """Syncronize two local directories recursively a la rsync"""

    # Could not get onerror in shutil.rmtree() to trigger so disabling this code
    # until I can figure out how to test it.
    # def removeReadOnly(fn, path, excinfo):
    #    try:
    #        os.chmod(path, stat.S_IWRITE)
    #    except Exception as e:
    #        log.warning('not removing {} because:\n{}'.format(path, e))

    if not path.exists(dstDir):
        os.makedirs(dstDir)

    for root, dirs, files in os.walk(srcDir):
        dstRootDir = path.join(dstDir, path.relpath(root, srcDir))
        if not path.exists(dstRootDir):
            os.makedirs(dstRootDir)
        for f in files:
            srcPath = path.join(srcDir, root, f)
            dstPath = path.join(dstRootDir, f)
            if not path.exists(dstPath) or not filecmp.cmp(
                srcPath, dstPath, shallow=False
            ):
                shutil.copy2(srcPath, dstPath)

    for root, dirs, files in os.walk(dstDir):
        srcRootDir = path.join(srcDir, path.relpath(root, dstDir))
        removed_dirs = []
        for d in dirs:
            if not path.exists(path.join(srcRootDir, d)):
                shutil.rmtree(path.join(root, d))   # , onerror=removeReadOnly)
                removed_dirs.append(d)
        for d in removed_dirs:
            dirs.remove(d)
        for f in files:
            if not path.exists(path.join(srcRootDir, f)):
                os.remove(path.join(root, f))


def rsync(src, dst):
    if platform.system() == 'Windows':
        cmd = ['scp', '-rp']
    else:
        cmd = ['rsync', '-a']
    if isinstance(src, (str, bytes, pathlib.Path)):
        cmd.append(src)
    else:
        assert hasattr(src, '__iter__'), 'source must be str, path or list'
        cmd.extend(src)
    cmd.append(dst)

    env = os.environ.copy()
    for key in list(env.keys()):
        if key.startswith('LC') or key.startswith('LANG'):
            del env[key]

    subprocess.run(cmd, env=env, check=True)


@contextlib.contextmanager
def ClosedTemporaryFile(*args, **kwargs):
    """Create and close a named temporary file"""
    delete = kwargs.pop('delete', True)
    kwargs['delete'] = False
    ftmp = tempfile.NamedTemporaryFile(*args, **kwargs)
    try:
        fpath = pathlib.Path(ftmp.name)
        ftmp.close()
        yield fpath
    finally:
        if delete:
            fpath.unlink()


def ExistingFile(p):
    """
    Asserts the path p is a file and returns it as a pathlib.Path object.
    """
    p = pathlib.Path(p).expanduser()
    if not p.is_file():
        msg = f'path {p} is not a file'
        log.error(msg)
        raise FileNotFoundError(msg)
    return p


def ExistingDir(p):
    """
    Asserts the path p is a directory and returns it as a pathlib.Path object.
    """
    p = pathlib.Path(p).expanduser()
    if not p.is_dir():
        msg = f'path {p} is not a directory'
        log.error(msg)
        raise FileNotFoundError(msg)
    return p


def Secret(s):
    p = pathlib.Path(s).expanduser()
    if p.is_file():
        if platform.system() != 'Windows' and p.stat().st_mode & 0o777 != 0o600:
            msg = 'file containing secret must have permissions set to 600'
            raise PermissionError(msg)
        s = p.read_text()
    return s.strip()


class tee:
    def __init__(self, filename, mode='a+t', pipe=sys.stderr, skip=0):
        self.filename = pathlib.Path(filename)
        self.mode = mode
        self.pipe = pipe
        self.skip = skip

    def __enter__(self):
        self.filename.parent.mkdir(parents=True, exist_ok=True)
        self.fout = open(self.filename, self.mode)
        self.count = 0
        if self.skip > 1 and self.pipe:
            self.pipe.write(f'[showing 1 out of every {self.skip} lines of output]\n')
            self.pipe.flush()
        return self

    def __exit__(self, *args, **kwargs):
        self.fout.close()
        if self.skip and self.pipe:
            self.pipe.write(
                colored(f'> tail -20 {self.filename}\n', 'yellow', attrs=['bold'])
            )
            out = self.filename.read_text().split('\n')[-20:]
            out = '\n'.join(out)
            self.pipe.write(out)
            if out[-1] != '\n':
                self.pipe.write('\n')
            self.pipe.flush()

    def write(self, msg):
        self.count += 1
        if self.pipe and (self.skip == 0 or (self.count % self.skip) == 0):
            self.pipe.write(msg)
            self.pipe.flush()
        self.fout.write(msg)
        self.fout.flush()
