import contextlib
import platform
import subprocess
import os


@contextlib.contextmanager
def virtual_display(display=':99', env=os.environ):
    if platform.system() == 'Linux':
        proc = subprocess.Popen(('Xvfb', display))
        try:
            env = env.copy()
            env['DISPLAY'] = display
            yield env
        finally:
            proc.kill()
    elif platform.system() == 'Windows':
        env = env.copy()
        env['MESA_GL_VERSION_OVERRIDE'] = '4.1COMPAT'
        yield env
    else:
        yield env


def run_in_virtual_display(cmd, *args, display=':99', env=os.environ, **kwargs):
    with virtual_display(display, env=env) and env:
        return subprocess.run(cmd, *args, env=env, **kwargs)
