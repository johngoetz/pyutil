#!/usr/bin/python3
"""
Script that creates Personal Access Token for Gitlab API;
Tested with:
- Gitlab Community Edition 10.1.4
- Gitlab Enterprise Edition 12.6.2
- Gitlab Enterprise Edition 13.4.4
"""
import argparse
import getpass
import logging
import os
import platform
import sys
import requests

from urllib.parse import urljoin
from bs4 import BeautifulSoup


log = logging.getLogger(__name__)


def find_csrf_token(text):
    soup = BeautifulSoup(text, "lxml")
    token = soup.find(attrs={"name": "csrf-token"})
    param = soup.find(attrs={"name": "csrf-param"})
    data = {param.get("content"): token.get("content")}
    return data


def obtain_csrf_token(root_route):
    r = requests.get(root_route)
    token = find_csrf_token(r.text)
    return token, r.cookies


def obtain_authenticity_token(pat_route, cookies):
    r = requests.get(pat_route, cookies=cookies)
    soup = BeautifulSoup(r.text, "lxml")
    attrs = {'name': 'csrf-token'}
    token = soup.find('meta', attrs=attrs).get('value')
    return token


def sign_in(login, password, sign_in_route, csrf, cookies):
    data = {
        "user[login]": login,
        "user[password]": password,
        "user[remember_me]": 0,
        "utf8": "✓"
    }
    data.update(csrf)
    r = requests.post(sign_in_route, data=data, cookies=cookies)
    token = find_csrf_token(r.text)
    return token, r.history[0].cookies


def obtain_personal_access_token(name, scopes, expires_at, csrf, pat_route, cookies, authenticity_token):
    data = {
        "personal_access_token[expires_at]": expires_at,
        "personal_access_token[name]": name,
        "personal_access_token[scopes][]": scopes,
        "authenticity_token": authenticity_token,
        "utf8": "✓"
    }
    data.update(csrf)
    r = requests.post(pat_route, data=data, cookies=cookies)
    token = json.loads(r.text)['new_token']
    return token


def generate_personal_access_token(gitlab_url, username, password, token_name, scopes, expires_at):
    root_route = urljoin(gitlab_url, "/")
    sign_in_route = urljoin(gitlab_url, "/users/sign_in")
    pat_route = urljoin(gitlab_url, "/-/profile/personal_access_tokens")
    csrf1, cookies1 = obtain_csrf_token(root_route)
    log.debug(f'root {csrf1} {cookies1}')
    csrf2, cookies2 = sign_in(username, password, sign_in_route, csrf1, cookies1)
    log.debug(f'sign_in {csrf2} {cookies2}')
    authenticity_token = obtain_authenticity_token(pat_route, cookies2)
    token = obtain_personal_access_token(token_name, scopes, expires_at, csrf2, pat_route, cookies2, authenticity_token)
    return token


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--gitlab-url', default=os.environ.get('GITLAB_URL', 'https://gitlab.com'),
        help='GitLab server HTTPS url')
    parser.add_argument(
        '-u', '--username', default=getpass.getuser(),
        help='login name for the GitLab server')
    parser.add_argument(
        '-p', '--password', help='login password for user')
    parser.add_argument(
        '-s', '--scope', default='api', type=lambda s: list(filter(None, s.split())),
        help='scope(s) for the generated personal access token (space separated)')
    parser.add_argument(
        '-e', '--expires-at', default='Never',
        help='token expiration date in the form "YYYY-MM-DD" or "Never"')
    parser.add_argument(
        'token_name', default=f'{{username}}@{platform.node()}', nargs='?',
        help='name of the generated access token default will be "{user}@{host}"')

    args = parser.parse_args()
    if not args.password:
        args.password = getpass.getpass(prompt=f'password for {args.username}: ')

    return args


def main():
    args = parse_args()
    token = generate_personal_access_token(args.gitlab_url, args.username, args.password, args.token_name, args.scope, args.expires_at)
    print(token)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    main()
