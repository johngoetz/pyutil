import contextlib
import sys


@contextlib.contextmanager
def syspath(directory):
    sys.path.insert(1, directory)
    try:
        yield
    finally:
        sys.path.pop(1)


@contextlib.contextmanager
def temporary_import(module, directory=None):
    ctx = syspath(directory) if directory else contextlib.nullcontext()
    with ctx:
        try:
            yield importlib.import_module(module)
        finally:
            if module in sys.modules:
                del sys.modules[module]
