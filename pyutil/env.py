import os


def join_paths(*paths):
    """joins paths into a single list using `os.pathsep` as seperator"""
    return os.pathsep.join(s for p in paths for s in p.split(os.pathsep) if s)


def append_paths_to_env(env, key, *paths):
    env[key] = join_paths(*([env.get(key, '')] + list(paths)))


def prepend_paths_to_env(env, key, *paths):
    """
    Usage from a list::

        >>> env = os.environ.copy()
        >>> paths = ['/a/b/c:/d/e/f', '/blah/la']
        >>> prepend_paths_to_env(env, 'PATH', *paths)

    from a string::

        >>> prepend_paths_to_env(env, 'PATH', '/my/path')
    """
    env[key] = join_paths(*(list(paths) + [env.get(key, '')]))


def env(key, *alternate_keys, default=None, environ=os.environ):
    """Fetch variables from environment, returning the first one or a default value."""
    if alternate_keys:
        return environ.get(key, env(*alternate_keys, default=default, environ=environ))
    else:
        return environ.get(key, default)
