import contextlib
import datetime
import logging
import os
import pathlib
import platform
import shlex
import subprocess
import sys
import textwrap
import time
import zipfile

import gitlab

from termcolor import colored

from .env import env
from .file_operations import tee
from .repository import GitRepository
from .virtual_display import virtual_display as vdisp
from .working_indicators import DataTransferProgressBar


log = logging.getLogger(__name__)
#log.setLevel(logging.DEBUG)


def echo(msg, color=None):
    sys.stdout.flush()
    sys.stderr.flush()
    sys.stdout.write(colored(msg, color, attrs=['bold']))
    if not msg.endswith('\n'):
        sys.stdout.write('\n')
    sys.stdout.flush()


def start_section(name, description, collapsed=True, stream=sys.stdout):
    sys.stdout.flush()
    sys.stderr.flush()
    if 'CI' in os.environ:
        time_stamp = int(time.time())
        desc = colored(description, 'magenta', attrs=['bold'])
        coll = '[collapsed=true]' if collapsed else ''
        stream.write(
            f'\u001b[0Ksection_start:{time_stamp}:{name}{coll}\r\u001b[0K{desc}\n'
        )
    else:
        stream.write(f'START SECTION: {name} ({description})\n')
    stream.flush()


def end_section(name, stream=sys.stdout):
    sys.stdout.flush()
    sys.stderr.flush()
    if 'CI' in os.environ:
        time_stamp = int(time.time())
        stream.write(f'\u001b[0Ksection_end:{time_stamp}:{name}\r\u001b[0K\n')
    else:
        stream.write(f'END SECTION: {name}\n')
    stream.flush()


@contextlib.contextmanager
def section(name, description, stream=sys.stdout):
    start_section(name, description, stream)
    try:
        yield
    finally:
        end_section(name, stream)


@contextlib.contextmanager
def logger(logfile):
    logfile = pathlib.Path(logfile)
    if not logfile.is_absolute():
        logdir = pathlib.Path(env('LOGDIR', default='.'))
        logfile = logdir / logfile
    with tee(logfile, stream=sys.stdout) as out:
        yield out


@contextlib.contextmanager
def popen(cmd, *, dry_run=False, check=False, **kwargs):
    try:
        if dry_run:
            print(shlex.join(cmd))
            cmd = (sys.executable, '-c', 'import sys;sys.exit(0)')
        if 'CI' in os.environ:
            echo(shlex.join(cmd), 'yellow')
        with subprocess.Popen(cmd, **kwargs) as proc:
            yield proc
        proc.wait()
        if check and proc.returncode != 0:
            raise subprocess.CalledProcessError(proc.returncode, cmd)
    finally:
        sys.stdout.flush()
        sys.stderr.flush()


def run(cmd, *, virtual_display=False, dry_run=False, env=os.environ, **kwargs):
    cwd = kwargs.get('cwd', None)
    shell = kwargs.get('shell', False)
    if not shell:
        cmd = list(map(str, cmd))
    try:
        if dry_run:
            if cwd:
                print(f'cd {cwd}')
            print(cmd if shell else shlex.join(cmd))
            return subprocess.run(
                (sys.executable, '-c', 'import sys;sys.exit(0)'),
                env=env,
                **kwargs,
            )
        if 'CI' in os.environ:
            if cwd:
                echo(f'pushd {cwd}', 'yellow')
            echo(cmd if shell else shlex.join(cmd), 'yellow')
        if virtual_display:
            with vdisp(env=env) as env:
                return subprocess.run(cmd, env=env, **kwargs)
        else:
            return subprocess.run(cmd, env=env, **kwargs)
    finally:
        if cwd:
            if 'CI' in os.environ:
                echo('popd', 'yellow')
            elif dry_run:
                print('cd -')
        sys.stdout.flush()
        sys.stderr.flush()


def run_remote(
    ssh_endpoint, cmd, *, check=False, dry_run=False, env=os.environ, **kwargs
):
    sshcmd = ('ssh', ssh_endpoint, 'bash', '-l', '-s')
    if not isinstance(cmd, str):
        cmd = shlex.join(cmd)
    if env:
        env = env.copy()
        for key in list(env.keys()):
            if key.startswith('LC') or key.startswith('LANG'):
                del env[key]
    kwargs.update(
        dry_run=dry_run,
        stdin=subprocess.PIPE,
        env=env,
        universal_newlines=True,
    )
    with popen(sshcmd, **kwargs) as proc:
        if 'CI' in os.environ:
            echo(f'<<< {cmd}', 'yellow')
        proc.communicate(cmd)
    if check and proc.returncode != 0:
        cmd = sshcmd + ['<<<', cmd]
        raise subprocess.CalledProcessError(proc.returncode, cmd)


def get_project(gl, name):
    projects = gl.projects.list(search=name, search_namespaces=True, iterator=True)
    projects = list(filter(lambda p: p.path_with_namespace == name, projects))
    if len(projects) > 1:
        raise ValueError(
            textwrap.dedent(
                f"""\
            found {len(projects)} projects matching {name}:
                {'  '.join([p.path_with_namespace for p in projects])}"""
            )
        )
    elif not projects:
        log.debug(f'project not found: {name}')
        return None
    return gl.projects.get(projects[0].id)


def branch_list(gl, working_dir, source_project=None):
    """Returns a list of (project_path, branch) tuples for searching artifacts.

    Parameters:
        gl ('gitlab.GitLab`): The GitLab instance.
        working_dir (`str`): A working directory within a git repository.
            This represents the target into which artifacts are to be downloaded.
        source_project (`str`): The GitLab server-side project path from which
            artifacts are to be downloaded.

    Example:
        gl = gitlab.Gitlab(gitlab_url, private_token=private_token)
        branches = branch_list(gl, 'products/tecplot')

    merge request ids will have a "!" prepended to differentiate them from
    pipeline id numbers.
    """
    try:
        repo = GitRepository(working_dir)
        project_path = repo.project_path
        current_branch = repo.branch
    except FileNotFoundError:
        project_path = working_dir
        current_branch = None

    default_branch = 'main'
    parent_project_path = None

    if gl:
        project = get_project(gl, project_path)
        if project:
            default_branch = project.default_branch
            try:
                parent_project_path = project.forked_from_project['path_with_namespace']
            except AttributeError as e:
                parent_project_path = None

    current_branch = env('CI_COMMIT_REF_NAME', default=current_branch or default_branch)

    production_branch = env(
        'PRODUCTION_BRANCH', 'CI_DEFAULT_BRANCH', default=default_branch
    )

    source_project_path = env(
        'CI_MERGE_REQUEST_SOURCE_PROJECT_PATH', default=project_path
    )
    source_branch = env('CI_MERGE_REQUEST_SOURCE_BRANCH_NAME', default=current_branch)

    target_project_path = env(
        'CI_MERGE_REQUEST_TARGET_PROJECT_PATH',
        'CI_MERGE_REQUEST_PROJECT_PATH',
        'CI_PROJECT_PATH',
    )
    target_branch = env(
        'CI_MERGE_REQUEST_TARGET_BRANCH_NAME',
        'CI_COMMIT_REF_NAME',
        default=production_branch,
    )

    possible_branches = []
    if not source_project:
        mrid = env('CI_MERGE_REQUEST_IID')
        if target_project_path and mrid:
            possible_branches.append((target_project_path, f'!{mrid}'))

    possible_branches.extend(filter(
        all,  # ensure path and branch are not None
        [
            (source_project, source_branch),
            (source_project_path, source_branch),
            (source_project, target_branch),
            (target_project_path, target_branch),
            (source_project, current_branch),
            (source_project_path, current_branch),
            (source_project, current_branch),
            (parent_project_path, current_branch),
            (source_project, production_branch),
            (parent_project_path, production_branch),
            (source_project, default_branch),
            (source_project_path, default_branch),
            (source_project, default_branch),
            (parent_project_path, default_branch),
            (source_project, 'main'),
            (source_project_path, 'main'),
            (source_project, 'main'),
            (parent_project_path, 'main'),
        ],
    ))

    # uniquify the branch list
    branches = []
    for b in possible_branches:
        if b not in branches:
            branches.append(b)

    return branches


def find_pipelines(gl, refs, status=None, max_age=None):
    """Return generator of pipelines that belong to all project:branch references.

    gl:
        The gitlab instance:
           gl = gitlab.Gitlab(gitlab_url, private_token=private_token)

    refs:
        refs are of the form ("project", "ref") where "ref" is a pipeline id number or
        branch name. A "!" may prepend the ref indicating to look for a merge request
        pipeline for the given branch or the latest pipeline of a specific merge request
        number.

        The merge request reference may be an id number or the source branch of the MR.

    status:
        May be `None` (all statuses) or a set of valid string status value.

    max_age:
        Consider only pipelines after a the given datetime based on age. Only used for
        branch references (not merge requests).
    """
    log.debug(f'find_pipelines({gl}, {refs}, {status})')
    for project_name, ref in refs:
        log.debug(f'project name: {project_name}')
        log.debug(f' ref: {ref}')
        if project := get_project(gl, project_name):
            log.debug(f'  project {project.id}')
            if ref.startswith('!'):
                try:
                    mrid = int(refs[1:])
                    if mr := project.mergerequests.get(mrid, state='opened'):
                        log.debug(f'   merge request {mrid}')
                        for mr_pipeline in mr.pipelines.list(source='merge_request_event', iterator=True):
                            if pipeline := project.pipelines.get(mr_pipeline.id):
                                log.debug(f'    pipeline {mr_pipeline.id}')
                                if status is None or pipeline.status in status:
                                    log.debug(f'        pipeline status {pipeline.status}')
                                    yield project, pipeline
                except:
                    branch = refs[1:]
                    for mr in project.mergerequests.list(state='opened', iterator=True):
                        if mr.source_branch == branch:
                            log.debug(f'   merge request {mr.id} (source branch: {branch})')
                            for mr_pipeline in mr.pipelines.list(source='merge_request_event', iterator=True):
                                if pipeline := project.pipelines.get(mr_pipeline.id):
                                    log.debug(f'    pipeline {mr_pipeline.id}')
                                    if status is None or pipeline.status in status:
                                        log.debug(f'        pipeline status {pipeline.status}')
                                        yield project, pipeline
            else:
                try:
                    pipeline_id = int(ref)
                    if pipeline := project.pipelines.get(pipeline_id):
                        log.debug(f'   pipeline {pipeline.id}')
                        yield project, pipeline
                except:
                    for pipeline in project.pipelines.list(ref=ref, iterator=True):
                        log.debug(f'   pipeline {pipeline.id}')
                        if max_age:
                            updated_at = datetime.datetime.strptime(
                                pipeline.updated_at, '%Y-%m-%dT%H:%M:%S.%f%z')
                            now = datetime.datetime.now(updated_at.tzinfo)
                            if updated_at < (now - max_age):
                                break
                        if status is None or pipeline.status in status:
                            log.debug(f'   pipeline status {pipeline.status}')
                            yield project, pipeline


def find_jobs(project, pipeline, refs, stage=None, status=None):
    """Returns jobs that match name or job id, stage and status within a pipeline.

    refs is a set of strings (job name) or integers (job id)

    stage is None (all stages) or a set of stage names

    status is None (all statuses) or a set of status identifier strings
    """
    log.debug(f'    find_job({project.path_with_namespace}, {pipeline.id}, {refs}, {stage}, {status})')
    for job in pipeline.jobs.list(iterator=True):
        if ref := refs.intersection({job.name, job.id}):
            if (stage is None or job.stage in stage) and (status is None or job.status in status):
                log.debug(f'         {job.stage} {job.name} {job.id}')
                if hasattr(job, 'artifacts_file'):
                    log.debug(f'      has artifacts file')
                    yield project.jobs.get(job.id)
                    refs -= ref
        if not refs:
            break


def download_artifacts_from_job(job, output, progress):
    log.info(f'downloading artifacts from job: {job.id} ({job.name})')
    log.info(f'artifact size: {int(job.artifacts_file["size"] / 2**20)} MB')
    log.info(f'job URL: {job.web_url}')

    tempfile = pathlib.Path('.artifacts.zip')
    artifacts_zipfile = tempfile if output.is_dir() else output

    with open(artifacts_zipfile, 'wb') as f:
        if progress:
            with DataTransferProgressBar(job.artifacts_file['size']) as progbar:
                def write(data, f=f, progbar=progbar):
                    f.write(data)
                    progbar.write(f.tell())
                job.artifacts(streamed=True, action=write)
        else:
            job.artifacts(streamed=True, action=f.write)

        log.info(f'downloaded artifacts to {artifacts_zipfile}')

    if output.is_dir():
        log.info(f'unzipping artifacts into {output}')
        if platform.system() == 'Windows':
            with zipfile.ZipFile(artifacts_zipfile) as fzip:
                fzip.extractall(output)
        else:
            # Not using Python's zipfile.extractall() because it doesn't
            # preserve symlinks, https://bugs.python.org/issue27318
            subprocess.run(('unzip', '-q', artifacts_zipfile, '-d', output), check=True)
        artifacts_zipfile.unlink()


def download_artifacts(
    gl,
    pipeline_refs,
    pipeline_status,
    job_refs,
    stage,
    status,
    output,
    progress,
    max_pipelines=100,
    max_age=datetime.timedelta(days=60),
):
    """Download artifacts and save off as a zip file or extract to directory.

    The artfacts are downloaded as a zip file and saved off to **output**
    unless **output** is an existing directory in which case the zip file
    is extracted at this location.

    All artifacts downloaded will be from a single pipeline but not all
    jobs may be found.

    The **gl** parameter is the gitlab instance:
        gl = gitlab.Gitlab(gitlab_url, private_token=private_token)
    """
    info = []
    for ref in pipeline_refs:
        for i, (project, pipeline) in enumerate(find_pipelines(gl, [ref], pipeline_status, max_age)):
            log.debug(f'{project.path_with_namespace} {pipeline}')
            if i > max_pipelines:
                raise LookupError(f'could not find valid job in past {max_pipelines} pipelines')
            for job in find_jobs(project, pipeline, job_refs, stage, status):
                log.info(f'branch: {project.path_with_namespace}:{job.ref}')
                download_artifacts_from_job(job, output, progress)
                info.append((project.path_with_namespace, job.ref, job.id))
            if info:
                break
    if not info:
        raise LookupError(f'could not find valid job with artifacts to download')
    return info
