import os
import pathlib
import shlex
import shutil
import subprocess
import sys


if sys.version_info < (3, 10):
    # back-port follow_symlinks argument for pathlib.Path.stat()
    def pathlib_Path_stat(self, follow_symlinks=True):
        return os.stat(self, follow_symlinks=follow_symlinks)

    pathlib.Path.stat = pathlib_Path_stat


if sys.version_info < (3, 9):
    # back-port pathlib.Path arguments for shutil.move()
    _shutil_move = shutil.move

    def shutil_move(src, dst, *args, **kwargs):
        return _shutil_move(str(src), str(dst), *args, **kwargs)

    shutil.move = shutil_move

    # back-port pathlib.Path.is_relative_to()
    def _path_is_relative_to(p, *other):
        try:
            _ = p.relative_to(*other)
            return True
        except ValueError:
            return False

    pathlib.Path.is_relative_to = _path_is_relative_to


# back-port and convert all arguments to string for shlex.join()
def shlex_join(split_command):
    return ' '.join(map(shlex.quote, map(str, split_command)))


shlex.join = shlex_join
