import math
import random as rand
import re
import shutil
import sys
import time


def terminal_width():
    tsize = shutil.get_terminal_size((80, 20))
    return tsize.columns


class Closable:
    def __enter__(self):
        return self

    def __exit__(self, type, value, tb):
        self.close()

    def close(self):
        raise NotImplementedError


class ProgressIndicator(Closable):
    def flush(self):
        sys.stderr.write('\n')
        sys.stderr.flush()

    def close(self):
        self.flush()


class ProgressDots(ProgressIndicator):
    """
    ```
        >>> with ProgressDots('dots') as prog:
        ...     for i in range(10):
        ...         prog.write()
        dots..........done.
    ```
    """

    def __init__(self, header=''):
        if header:
            sys.stderr.write(header)
            sys.stderr.flush()
        self.last_update = None

    def write(self, *_):
        now = time.monotonic()
        if self.last_update is None or now - self.last_update > 0.1:
            self.last_update = now
            sys.stderr.write('.')
            sys.stderr.flush()

    def close(self):
        sys.stderr.write('done.\n')


class ProgressBall(ProgressIndicator):
    """
    ```
        >>> with ProgressBall('ball') as prog:
        ...     for i in range(10):
        ...         prog.write()
        ball [-----o---------------]
    ```
    """

    def __init__(self, header=''):
        self.ypad = 4 + len(header)
        self.ymin = 3

        self.fmt = '\r'
        if header:
            self.fmt += header + ' '
        self.fmt += '[{ball}]'

        self.ball = 'o'
        self.fill = '-'
        self.count = 0
        self.last_update = None

    def write(self, *_):
        now = time.monotonic()
        if self.last_update is None or now - self.last_update > 0.1:
            w = min(terminal_width() - self.ypad, 20)
            if w < self.ymin:
                if now - self.last_update > 1:
                    self.last_update = now
                    sys.stderr.write('.')
                    sys.stderr.flush()
            else:
                self.last_update = now
                self.count += 1
                i = abs((self.count % (2 * w)) - w)
                ball = (self.fill * (w - i)) + self.ball + (self.fill * i)
                sys.stderr.write(self.fmt.format(ball=ball))
                sys.stderr.flush()


class ProgressWave(ProgressIndicator):
    """
    ```
        >>> with ProgressWave('wave') as prog:
        ...     for i in range(10):
        ...         prog.write()
        wave ˙ॱ⋅.˳˳.⋅ॱ˙˙ॱ⋅.˳˳.⋅ॱ˙
    ```
    """

    def __init__(self, header=''):
        self.ypad = 2 + len(header)
        self.ymin = 10

        self.fmt = '\r'
        if header:
            self.fmt += header + ' '
        self.fmt += '{wave}'

        self.wave = '˙ॱ⋅.˳˳.⋅ॱ˙'
        self.count = 0
        self.last_update = None

    def width(self):
        max_width = terminal_width() - self.wpad
        return min(max_width, 30)

    def write(self, *_):
        now = time.monotonic()
        if self.last_update is None or now - self.last_update > 0.05:
            w = min(terminal_width() - self.ypad, 20)
            if w < self.ymin:
                if now - self.last_update > 1:
                    self.last_update = now
                    sys.stderr.write('.')
                    sys.stderr.flush()
            else:
                self.last_update = now
                self.count -= 1
                nwaves = int(math.ceil(w / len(self.wave)))
                wave = ''.join(
                    [self.wave[self.count % len(self.wave) :]] + [self.wave] * nwaves
                )[:w]
                sys.stderr.write(self.fmt.format(wave=wave))
                sys.stderr.flush()


def format_with_scale(value):
    if value > 0:
        scale = 3 * min(max(int(math.log10(value) // 3), -12), 12)
        value /= 10**scale
        if value >= 1000:
            scale += 3
            value /= 1000
        unit = {
            -12: 'p',
            -9: 'n',
            -6: 'u',
            -3: 'm',
            0: ' ',
            3: 'K',
            6: 'M',
            9: 'G',
            12: 'T',
        }[scale]
        if value < 10:
            return f'{value:>3.1f}{unit}'
        else:
            return f'{int(value):>3d}{unit}'
    else:
        return '  0 '


class ProgressBar(ProgressIndicator):
    """
    ```
        >>> with ProgressBar('bar', footer='footer') as prog:
        ...     for i in range(101):
        ...         prog.write(i)
        bar [==========================] 100% footer
    ```

    ```
        >>> start = time.time()
        >>> total = 200
        >>> with ProgressBar(header='rate',
        ...                 footer=f'{total}MB ({{rate:>3d}}MB/s)',
        ...                 pad=4 + 15) as prog:
        ...    for i in range(201):
        ...        prog.write(100 * i / 200, rate=int(i / (time.time() - start)))
        rate [=========================] 100% 200MB ( 96MB/s)
    ```
    """

    def __init__(
        self,
        progress_pattern=r'.*?(\d+)\s*%.*',
        header='',
        footer='',
        pad=None,
        min_value=0,
        max_value=100,
    ):
        assert min_value < max_value, 'min value must be less than max'

        self.ypad = 10 + (pad or (len(header) + len(footer)))
        self.ymin = 5

        self.min_value = min_value
        self.max_value = max_value
        self.start = time.monotonic()
        self.rate = 0

        self.fmt = '\r'
        if header:
            self.fmt += header + ' '
        self.fmt += '[{bar}{space}] {percent:>3}%'
        if footer:
            self.fmt += ' ' + footer

        self.percent = -1
        self.ptrn = re.compile(progress_pattern) if progress_pattern else None

    def interpret_message(self, msg):
        if isinstance(msg, str) and self.ptrn:
            m = self.ptrn.match(msg)
            if m:
                value = m.group(1)
            else:
                return self.percent
        else:
            value = msg
        value = min(max(value, self.min_value), self.max_value)
        rate = (value - self.min_value) / (time.monotonic() - self.start)
        ratio = (value - self.min_value) / (self.max_value - self.min_value)
        percent = min(max(int(100 * ratio), 0), 100)
        return percent, value, rate

    def write(self, msg, **kwargs):
        percent, value, rate = self.interpret_message(msg)
        if self.percent != percent:
            self.percent = percent
            w = min(terminal_width() - self.ypad, 60)
            if w < self.ymin:
                sys.stderr.write(f'{self.percent:>3}%\n')
                sys.stderr.flush()
            else:
                ww = 2 * w
                prog = int(percent * ww / 100)
                bar = ('=' * (prog // 2)) + ('-' * (prog % 2))
                space = ' ' * (w - len(bar))
                opts = dict(
                    bar=bar,
                    space=space,
                    percent=percent,
                    value=format_with_scale(value),
                    rate=format_with_scale(rate),
                )
                opts.update(**kwargs)
                sys.stderr.write(self.fmt.format(**opts))
                sys.stderr.flush()


class DataTransferProgressBar(ProgressBar):
    """
    ```
        >>> size = 2e9
        >>> with DataTransferProgressBar(header='download', size=size) as prog:
        ...     for i in range(200+1):
        ...         prog.write(i * (size / 200))
        download [========-        ]  55% 1.1GB / 2.0GB (984MB/s)
    ```
    """

    def __init__(self, size, header=''):
        footer = f'{{value}}B / {format_with_scale(size)}B ({{rate}}B/s)'
        pad = len(header) + 23
        super().__init__(header=header, footer=footer, pad=pad, max_value=size)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        'name',
        nargs='?',
        default='all',
        choices=('dots', 'ball', 'wave', 'bar', 'rate', 'rate', 'data', 'all'),
    )
    args = parser.parse_args()

    if args.name in {'dots', 'all'}:
        with ProgressDots('dots') as prog:
            for i in range(10):
                prog.write()
                time.sleep(0.1)

    if args.name in {'ball', 'all'}:
        with ProgressBall('ball') as prog:
            for i in range(45):
                prog.write()
                time.sleep(0.1)

    if args.name in {'wave', 'all'}:
        with ProgressWave('wave') as prog:
            for i in range(10):
                prog.write()
                time.sleep(0.1)

    if args.name in {'bar', 'all'}:
        with ProgressBar(header='bar', footer='footer') as prog:
            for i in range(201):
                prog.write(100 * i / 200)
                time.sleep(0.01)

    if args.name in {'rate', 'all'}:
        start = time.time()
        total = 200
        with ProgressBar(
            header='rate', footer=f'{total}MB ({{rate:>3d}}MB/s)', pad=4 + 15
        ) as prog:
            for i in range(201):
                prog.write(100 * i / 200, rate=int(i / (time.time() - start)))
                time.sleep(max(0.001, rand.gauss(0.01, 0.002)))

    if args.name in {'data', 'all'}:
        size = 200
        with DataTransferProgressBar(header='download', size=size) as prog:
            for i in range(size + 1):
                prog.write(i)
                time.sleep(max(0.001, rand.gauss(0.01, 0.002)))

        size = 2e9
        with DataTransferProgressBar(header='download', size=size) as prog:
            for i in range(200 + 1):
                prog.write(i * (size / 200))
                time.sleep(max(0.001, rand.gauss(0.01, 0.002)))
