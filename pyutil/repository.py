import getpass
import pathlib
import re
import subprocess


def git_enable_symlinks():
    subprocess.run(('git', 'config', 'core.symlinks', 'true'), check=True)


def is_git_repo(subdir):
    cmd = ('git', '-C', subdir, 'branch', '--show-current')
    proc = subprocess.run(cmd, capture_output=True)
    return proc.returncode == 0


def git_superproject_dir(subdir):
    subdir = pathlib.Path(subdir).expanduser()
    cmd = (
        'git',
        '-C',
        subdir,
        'rev-parse',
        '--show-superproject-working-tree',
    )
    try:
        return subprocess.check_output(cmd, text=True, stderr=subprocess.DEVNULL).strip()
    except:
        return None


def git_topdir(subdir, superproject=True):
    subdir = pathlib.Path(subdir).expanduser()
    try:
        if superproject:
            superdir = git_superproject_dir(subdir)
            while superdir:
                subdir = superdir
                superdir = git_superproject_dir(subdir)
        cmd = ('git', '-C', subdir, 'rev-parse', '--show-toplevel')
        out = subprocess.check_output(cmd, text=True, stderr=subprocess.DEVNULL).strip()
        return pathlib.Path(out)
    except:
        return subdir


class GitRepository:
    def __init__(self, subdir, superproject=True):
        self.subdir = pathlib.Path(subdir).expanduser()
        if not self.subdir.is_dir():
            raise FileNotFoundError(f'{subdir} is not a directory')
        self.topdir = git_topdir(self.subdir, superproject)

    def git(self, *args):
        cmd = ['git', '-C', self.topdir] + list(args)
        try:
            return subprocess.check_output(cmd, text=True, stderr=subprocess.DEVNULL).strip()
        except subprocess.CalledProcessError:
            return None

    @property
    def commit(self):
        return self.git('rev-parse', '--short', 'HEAD')

    @property
    def branch(self):
        return self.git('rev-parse', '--abbrev-ref', 'HEAD')

    @property
    def remote(self):
        branch = self.branch
        if branch:
            return self.git('config', f'branch.{branch}.remote')

    @property
    def remote_url(self):
        remotes = (self.remote, getpass.getuser(), 'origin', 'upstream')
        for remote in filter(None, remotes):
            remote_url = self.git('remote', 'get-url', remote)
            if remote_url:
                return remote_url

    @property
    def server_address(self):
        url = self.remote_url
        if url:
            if '://' in url:
                addr = url.split('://')[-1]
                if '@' in addr:
                    addr = addr.split('@')[-1]
                addr = addr.split(':')[0]
                addr = addr.split('/')[0]
            elif ':' in url:
                addr = url.split(':')[0]
            else:
                addr = None
            if '@' in addr:
                addr = addr.split('@')[-1]
            return addr

    @property
    def project_path(self):
        remote_url = self.remote_url
        if remote_url:
            ptrn = re.compile(r'.*(?:@|://)[\w\.]*?(?::|/)(.*?)(?:\.git)?$')
            return ptrn.match(remote_url).group(1)

    @property
    def namespace(self):
        project_path = self.project_path
        if project_path:
            return project_path.rsplit('/', maxsplit=1)[0]

    @property
    def project(self):
        project_path = self.project_path
        if project_path:
            return project_path.rsplit('/', maxsplit=1)[-1]

    def has_local_changes(self, subdir='.'):
        return self.git('diff-index', '--quiet', 'HEAD', subdir) is None

    def commit_count_to_branch(self, branch):
        return int(self.git('rev-list', '--count', f'...{branch}') or 1e9)
