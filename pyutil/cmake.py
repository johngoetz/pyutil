def as_bool(value):
    """Convert value to boolean, interpreting strings such as yes and no.

    Any value that Python will convert to False returns False. For all other values
    such as integers, strings and strings that can be converted to integers, the
    following rules apply: non-zero integers return True, strings that start with "y"
    or "t" and the string "on" all return True. All other values return False ("blah"
    returns False for example).
    """
    if value:
        if isinstance(value, bool):
            return value
        elif isinstance(value, str):
            value = value.lower()
            if value[0] in {'y', 't'} or value == 'on':
                return True
            else:
                try:
                    return bool(int(value))
                except ValueError:
                    return False
        else:
            return bool(value)
    else:
        return False


def cmake_bool(value):
    """Interpret value as a boolean and return the CMake identifier."""
    return 'TRUE' if as_bool(value) else 'FALSE'
