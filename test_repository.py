import unittest

import pyutil


class TestGitRepository(unittest.TestCase):
    def test_server_address(self):
        class MockGitRepository(pyutil.GitRepository):
            def __init__(self):
                pass
            @property
            def remote_url(self):
                return self._remote_url
            @remote_url.setter
            def remote_url(self, url):
                self._remote_url = url

        repo = MockGitRepository()

        repo.remote_url = 'git@git.domain.com:user/project.git'
        self.assertEqual(repo.server_address, 'git.domain.com')

        repo.remote_url = 'https://user:private-token@git.domain.com/user/project.git'
        self.assertEqual(repo.server_address, 'git.domain.com')

        repo.remote_url = 'https://user@git.domain.com/user/project.git'
        self.assertEqual(repo.server_address, 'git.domain.com')

        repo.remote_url = 'https://git.domain.com/user/project.git'
        self.assertEqual(repo.server_address, 'git.domain.com')


if __name__ == '__main__':
    import test

    test.main()
