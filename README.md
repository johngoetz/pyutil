[[_TOC_]]

# PyUtil: General Python Utilities

This is a collection of useful though unrelated operations in support of other
Python packages. This can be added to a package's `setup.cfg`, notice this
requires Python 3.6 or later:

```ini
[options]
python_requires = >=3.6
install_requires =
    pyutil @ git+https://gitlab.com/johngoetz/pyutil.git
```

This package may be installed specifically via `pip`:

```shell
pip install git+https://gitlab.com/johngoetz/pyutil.git
```

# Back-ported Python core features

upon import of the `pyutil` module, the following features are back-ported to
at least Python 3.6.

* `subprocess.run()` arguments: `capture_output` and `text`.
* `shutil.move()` accepts `pathlib.Path` objects.
* added `shlex.join()` and implicitly convert all input to strings.

# `appdirs`: Application and User Directories

`appdirs` is guaranteed to have the following methods for Darwin, Linux and
Windows. `app` is the application name, `org` is the organization name and
`qual` is the organization qualifier (like "com" or "org").

```python
from pyutil import appdirs

appdirs.cache_dir(app, org, qual)
appdirs.config_dir(app, org, qual)
appdirs.data_dir(app, org, qual)
appdirs.local_data_dir(app, org)
appdirs.remote_data_dir(app, org)
appdirs.runtime_dir(app)
```

The `qual` parameter is ignored on Linux and Windows and the `org` parameter is
ignored on Linux. For all platforms, both `org` and `qual` may be omitted. On
Linux and Darwin systems, `data_dir()`, `local_data_dir()` and
`remote_data_dir()` are equivalent. On Windows and Darwin, `runtime_dir()` is
equivalent to `cache_dir()`.

Examples:

```
appdirs.config_dir('App', 'org', 'qual')
```

* Windows: `C:\Users\user\AppData\Roaming\Config\org\App`
* Linux:   `/home/user/.config/App`
* Darwin:  `/Users/user/Library/Preferences/qual.org.App`

```
appdirs.data_dir('App', 'org', 'qual')
```

* Windows: `C:\Users\user\AppData\Local\org\App`
* Linux:   `/home/user/.local/share/App`
* Darwin:  `/Users/user/Library/Appplication Support/qual.org.App`

# CMake Utilities: `as_bool()`, `cmake_bool()`

The `as_bool()` function converts any value to boolean, interpreting strings
such as yes and no.

Any value that Python will convert to `False` returns `False`. For all other
values such as integers, strings and strings that can be converted to integers,
the following rules apply: non-zero integers return `True`, strings that start
with "y" or "t" and the string "on" all return `True`. All other values return
`False` ("blah" returns `False` for example).

The `cmake_bool()` function converts any value to one of CMake's boolean
identifier strings "TRUE" or "FALSE". This uses `as_bool()` under the hood
so that strings such as "yes" and "no" are interpreted correctly.

# `CommandParser()`: Sub-Command Argument Parser

This is an enhancement to Python's `argparse.ArgumentParser` that provides
a convenient way to add sub-commands, allowing for more flexible optional
argument placement on the command-line.

Example usage:

```python
def init(args):
    print(args)

def push(args):
    print(args)

parser = pyutil.CommandParser('program')
parser.add_config_file(appdirs.config_dir('app')/'config')
parser.add_version_argument(__version__)
parser.add_verbosity_arguments()
parser.add_argument('-o', '--opt', help='global option')

initparser = parser.add_command('init', func=init, help='init')
initparser.add_argument('input', help='init input')

pushparser = parser.add_command('push', func=push, help='push')
pushparser.add_argument('-o', '--output', help='push output')
```

Values for arguments follow these rules:

```
command line > environment > config file > defaults
```

Defaults are overridden by the config file which are overridden by environment
variables -- replace "program" with the name of the program as given to the
`CommandParser` constructor:

```ini
[program]
global-key = value
[command]
key = value
[command.subcommand]
key = value
```

and all environment variables that look like this will be honored:

```shell
PROGRAM_GLOBAL_KEY=value
PROGRAM_COMMAND_KEY=value
PROGRAM_COMMAND_SUBCOMMAND_KEY=value
```

# Environment Utilities

The following will append or prepend paths to an variable (key) in the
environment in-place using the current system's path separator (`os.pathsep`):

* `append_paths_to_env(env, key, *paths)`
* `prepend_paths_to_env(env, key, *paths)`

The function `join_paths(*paths)` joins paths using `os.pathsep`.

The `env()` function looks in the environment for the first match of several
environment variables. It will optionally return a default value:

* `env(key, *alternate_keys, default=None, environ=os.environ)`

# File Operations

## `synctree(srcDir, dstDir)`

Syncronize two local directories recursively a la rsync. Files that are not in
the source directory (`srcDir`) are deleted from the destination (`dstDir`).

## `rsync(src, dst)`

Syncronize two directories on different machines.

## `ClosedTemporaryFile()`

Context manager to create a temporary file but have it closed during the body
of the context:

```python
with pyutil.ClosedTemporaryFile() as ftmp:
    with open(ftmp, 'a+t') as fout:
        fout.write('testing\n')
    assert ftmp.read_text() == 'testing\n'
```

## `ExistingDir()`

Converts a string to a `pathlib.Path()` object and asserts that it is an
existing directory. This is for use in command line argument parsing:

```python
parser.add_argument('--outdir', type=pyutil.ExistingDir)
```

## `Secret()`

Reads a private key or secret from a file which must have permissions set to
`0o600` on Linux and Darwin. Strings are stripped of leading and trailing
whitespace and passed through. This is typically for use in command line
argument parsing of files containing sensitive information:

```python
parser.add_argument('--password', type=pyutil.Secret, default='~/.password')
```

## `tee()`

Context manager to split output to a file while also writing to a stream
(sys.stdout by default). This mimicks the behavior of Linux's `tee` command.
The following example will output the message to the file `operation.log' as
well as to `stdout`:

```python
with tee('operation.log') as out:
    out.write('progress message\n')
```

# GitLab Operations

## `gitlab.download_artifacts()`

Downloads artifacts from GitLab-CI jobs.

## `gitlab.get_project()`, `gitlab.find_job()`

Gets a project or job object representing a project or job in the GitLab-CI
system.

## `run(cmd, *, virtual_display=False, dry_run=False, env=os.environ, **kwargs)`

Run a subprocess, optionally in a virtual display on Linux, printing out the
command using `gitlab.echo` if running in a GitLab-CI environment -- that is,
if "CI" environment variable is set in `os.environ`. The `cmd`, `env` and
`**kwargs` arguments are forwarded to `subprocess.run()`.

## `gitlab.section(name, description, stream=sys.stdout)`

Context manager to print section begin and end messages for the GitLab-CI
console. Generic messages are printed to the stream if not in a CI environment:

```python
with pyutil.gitlab.section('build', 'Build the product'):
    build(project)
```

## `gitlab.echo(msg, color=None)`

Print the message with color if specified. The `color` argument may be one of:
`red`, `green`, `yellow`, `blue`, `magenta`, `cyan`. This is for use with the
GitLab-CI console output -- that is, writing to `stdout` during a CI job.

# Repository Operations

## `git_enable_symlinks()`

Globally enables symlinks in a git repository.

## `is_git_repo(subdir)`

Returns `True` if the `subdir` is within a git repository.

## `git_topdir(subdir)`

Returns the top-level of the git repository's ultimate super-project as a
`pathlib.Path` object.

## `GitRepository`

A git repository object to query the top-level directory and project path.

```
repo = pyutil.GitRepository('.')
```

Repository properties:

* `repo.subdir`: Directory passed to `GitRepository()`
* `repo.topdir`: Top-level directory in the ultimate super-project if `subdir`
  is within any submodules.
* `repo.branch`: The currently tracked branch name.
* `repo.remote`: The name of the remote of the currently tracked branch.
* `repo.remote_url`: The URL of the remote.
* `repo.server_address`: The address part of the remote URL.
* `repo.project_path`: The full path to the project in the current remote URL.
  For paths such as `https://gitlab.com/johngoetz/pyutil.git`, this will be
  `johngoetz/pyutil`.
* `repo.namespace`: The group name of the current project -- `johngoetz` in the
  example above.
* `repo.project`: The project name -- `pyutil` in the example above.

## `pyutil.virtual_display()`: Context manager for running in `Xvfb`

On Linux, this starts up an instance of `Xvfb` which must be installed on the
system. It yields a modified environment with the appropriate value for the
`DISPLAY` variable:

```python
with pyutil.virtual_display() as env:
    subprocess.run(cmd, env=env)
```

The above example is the same as calling `pyutil.run_in_virtual_display(cmd)`.

# Python Import Operations

## `import_from_file(filename)`

Imports a python source file or directory as a module. Example:

```
util = pyutil.import_from_file('util')
```
