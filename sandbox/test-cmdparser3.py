import pyutil
import sys

def init(args):
    print('init')
    print(args)

def push(args):
    print('push')
    print(args)

parser = pyutil.CommandParser('test')

parser.add_argument('-a', '--aaa', action='count', help='aaa help')
parser.add_argument('-b', '--bbb', action='store_true', help='bbb help')
parser.add_argument(
    '--config', default='~/.config/test', help='configuration file'
)

initparser = parser.add_command('init', func=init, help='initialize something')
initparser.add_argument('-c', '--ccc', action='count', help='init ccc help')
initparser.add_argument('-d', '--ddd', action='store_true', help='ddd help')
initparser.add_argument('-e', '--eee', default='-', help='eee help')

initparser.add_argument('input', help='input file or - for stdin')

pushparser = parser.add_command('push', func=push, help='push something')
pushparser.add_argument('-c', '--ccc', action='count', help='push ccc help')
pushparser.add_argument('-f', '--fff', action='store_true', help='fff help')
pushparser.add_argument('-e', '--egg', default='-', help='egg help')
pushparser.add_argument('input', help='input file or - for stdin')


print(parser._zsh_autocomplete())
args = parser.parse_args()
print(args)
sys.exit(args.func(args) or 0)
