import sys
import pyutil

# import logging
# logging.basicConfig(level=logging.DEBUG)


def subcmd(args):
    print('subcmd')


parser = pyutil.CommandParser('prog')
parser.add_argument('-a', '--aa', action='count')

cmdparser = parser.add_command('cmd')
cmdparser.add_argument('-b', '--bb', action='count')
cmdparser.add_argument('-c', '--cc', action='count')

subcmdparser = cmdparser.add_command('subcmd', func=subcmd)
subcmdparser.add_argument('-d', '--dd', action='count')

args = parser.parse_args()
print(args)
sys.exit(args.func(args) or 0)
