import sys

import pyutil


__version__ = '1.2.3'


def setup_cmd(args):
    print('setup')


def setup_repo_cmd(args):
    print('setup repo')


def setup_env_cmd(args):
    print('setup env')


def run_debug_cmd(args):
    print('run debug')


def run_release_cmd(args):
    print('run release')


def parse_args():
    parser = pyutil.CommandParser()
    parser.add_version_argument(__version__)
    parser.add_config_file_argument()
    parser.add_verbosity_arguments()
    parser.add_argument('--one', help='global config one')

    setup_parser = parser.add_command('setup', func=setup_cmd, help='set something up')
    setup_parser.add_argument('--two', help='setup option two')

    setup_repo_parser = setup_parser.add_command(
        'repo', func=setup_repo_cmd, help='setup the repo'
    )
    setup_repo_parser.add_argument('--three')
    setup_repo_parser.add_argument('target', nargs='?')

    setup_env_parser = setup_parser.add_command(
        'env', func=setup_env_cmd, help='setup the environment'
    )
    setup_env_parser.add_argument('--four')
    setup_env_parser.add_argument('target')

    run_parser = parser.add_command('run', help='run it')
    run_parser.add_argument('--five', help='option five for run')

    run_debug_parser = run_parser.add_command(
        'debug', func=run_debug_cmd, help='debug something'
    )
    run_debug_parser.add_argument('--six')

    run_release_parser = run_parser.add_command(
        'release', func=run_release_cmd, help='run it in release mode'
    )
    run_release_parser.add_argument('--seven')
    run_release_parser.add_argument('target', nargs='*')

    return parser.parse_args()


def main():
    args = parse_args()
    print(args)
    return args.func(args)


if __name__ == '__main__':
    sys.exit(main() or 0)
