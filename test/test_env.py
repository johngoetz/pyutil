import os
import unittest

import pyutil


class TestEnv(unittest.TestCase):
    def test_join_paths(self):
        a = os.pathsep.join(['/one/two', 'a/b'])
        b = os.pathsep.join(['1234', '/one/two'])

        expected = os.pathsep.join(['/one/two', 'a/b', '1234', '/one/two'])
        self.assertEqual(expected, pyutil.join_paths(a, b))

    def test_append_paths_to_env(self):
        env = {'TEST': os.pathsep.join(['/one/two', 'a/b'])}
        a = os.pathsep.join(['1234', '/one/two'])

        expected = os.pathsep.join(['/one/two', 'a/b', '1234', '/one/two'])
        pyutil.append_paths_to_env(env, 'TEST', a)
        self.assertEqual({'TEST': expected}, env)

    def test_prepend_paths_to_env(self):
        env = {'TEST': os.pathsep.join(['/one/two', 'a/b'])}
        a = os.pathsep.join(['1234', '/one/two'])

        expected = os.pathsep.join(['1234', '/one/two', '/one/two', 'a/b'])
        pyutil.prepend_paths_to_env(env, 'TEST', a)
        self.assertEqual({'TEST': expected}, env)

    def test_env(self):
        environ = {'TEST': 'testing'}

        result = pyutil.env('TEST', default='not-used', environ=environ)
        self.assertEqual('testing', result)

        result = pyutil.env('NOTSET', default='default-value', environ=environ)
        self.assertEqual('default-value', result)

    def test_alternate_keys(self):
        environ = {'TEST': 'testing'}
        result = pyutil.env('TEST', 'TEST_ALT', default='not-used', environ=environ)
        self.assertEqual('testing', result)

        environ = {'TEST_ALT': 'testing_alt'}
        result = pyutil.env('TEST', 'TEST_ALT', default='not-used', environ=environ)
        self.assertEqual('testing_alt', result)

        environ = {'TEST_NODEF': 'testing_nodef'}
        result = pyutil.env('TEST', 'TEST_ALT', default='not-used', environ=environ)
        self.assertEqual('not-used', result)


if __name__ == '__main__':
    import test

    test.main()
