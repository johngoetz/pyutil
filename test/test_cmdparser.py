import contextlib
import io
import re
import unittest

import pyutil


class TestCommandParserTwoLevel(unittest.TestCase):
    def setUp(self):
        def subcmd(args):
            return args.aa, args.bb, args.cc, args.dd

        self.parser = pyutil.CommandParser('prog')
        self.parser.add_argument('-a', '--aa', action='count')

        cmdparser = self.parser.add_command('cmd')
        cmdparser.add_argument('-b', '--bb', action='count')
        cmdparser.add_argument('-c', '--cc', action='count')

        subcmdparser = cmdparser.add_command('subcmd', func=subcmd)
        subcmdparser.add_argument('-d', '--dd', action='count')

        self.parser._build_parser([], None)

    def test_all_short_options(self):
        args = self.parser.parse_args('-a cmd -abc subcmd -abcd'.split())
        self.assertEqual(args.aa, 3)
        self.assertEqual(args.bb, 2)
        self.assertEqual(args.cc, 2)
        self.assertEqual(args.dd, 1)

        args = self.parser.parse_args('-a cmd -cba subcmd -dcba'.split())
        self.assertEqual(args.aa, 3)
        self.assertEqual(args.bb, 2)
        self.assertEqual(args.cc, 2)
        self.assertEqual(args.dd, 1)

        args = self.parser.parse_args('-a cmd subcmd'.split())
        self.assertEqual(args.aa, 1)
        self.assertIsNone(args.bb)
        self.assertIsNone(args.cc)
        self.assertIsNone(args.dd)

        args = self.parser.parse_args('cmd -b subcmd'.split())
        self.assertIsNone(args.aa)
        self.assertEqual(args.bb, 1)
        self.assertIsNone(args.cc)
        self.assertIsNone(args.dd)

        args = self.parser.parse_args('cmd subcmd -d'.split())
        self.assertIsNone(args.aa)
        self.assertIsNone(args.bb)
        self.assertIsNone(args.cc)
        self.assertEqual(args.dd, 1)

    def test_all_long_options(self):
        args = self.parser.parse_args(
            '--aa cmd --aa --bb --cc subcmd --aa --bb --cc --dd'.split()
        )
        self.assertEqual(args.aa, 3)
        self.assertEqual(args.bb, 2)
        self.assertEqual(args.cc, 2)
        self.assertEqual(args.dd, 1)

        args = self.parser.parse_args(
            '--aa cmd --cc --bb --aa subcmd --dd --cc --bb --aa'.split()
        )
        self.assertEqual(args.aa, 3)
        self.assertEqual(args.bb, 2)
        self.assertEqual(args.cc, 2)
        self.assertEqual(args.dd, 1)

        args = self.parser.parse_args('--aa cmd subcmd'.split())
        self.assertEqual(args.aa, 1)
        self.assertIsNone(args.bb)
        self.assertIsNone(args.cc)
        self.assertIsNone(args.dd)

        args = self.parser.parse_args('cmd --bb subcmd'.split())
        self.assertIsNone(args.aa)
        self.assertEqual(args.bb, 1)
        self.assertIsNone(args.cc)
        self.assertIsNone(args.dd)

        args = self.parser.parse_args('cmd subcmd --dd'.split())
        self.assertIsNone(args.aa)
        self.assertIsNone(args.bb)
        self.assertIsNone(args.cc)
        self.assertEqual(args.dd, 1)

    def test_short_and_long_options(self):
        args = self.parser.parse_args('--aa cmd -ab --cc subcmd --dd -bac'.split())
        self.assertEqual(args.aa, 3)
        self.assertEqual(args.bb, 2)
        self.assertEqual(args.cc, 2)
        self.assertEqual(args.dd, 1)

    def test_no_options(self):
        args = self.parser.parse_args('cmd subcmd'.split())
        self.assertIsNone(args.aa)
        self.assertIsNone(args.bb)
        self.assertIsNone(args.cc)
        self.assertIsNone(args.dd)

    def test_unknown_short_options(self):
        def error(msg):
            raise RuntimeError(msg)

        _e = self.parser.parser.error
        self.parser.parser.error = error
        try:
            with self.assertRaisesRegex(RuntimeError, 'unrecognized arguments: -e'):
                args = self.parser.parse_args('-e cmd subcmd'.split())
            with self.assertRaisesRegex(RuntimeError, 'unrecognized arguments: -e'):
                args = self.parser.parse_args('cmd -e subcmd'.split())
            with self.assertRaisesRegex(RuntimeError, 'unrecognized arguments: -e'):
                args = self.parser.parse_args('cmd subcmd -e'.split())
        finally:
            self.parser.parser.error = _e

    def test_unknown_long_options(self):
        def error(msg):
            raise RuntimeError(msg)

        _e = self.parser.parser.error
        self.parser.parser.error = error
        try:
            with self.assertRaisesRegex(RuntimeError, 'unrecognized arguments: --ee'):
                args = self.parser.parse_args('--ee cmd subcmd'.split())
            with self.assertRaisesRegex(RuntimeError, 'unrecognized arguments: --ee'):
                args = self.parser.parse_args('cmd --ee subcmd'.split())
            with self.assertRaisesRegex(RuntimeError, 'unrecognized arguments: --ee'):
                args = self.parser.parse_args('cmd subcmd --ee'.split())
        finally:
            self.parser.parser.error = _e


class TestCommandParserNoCommands(unittest.TestCase):
    def setUp(self):
        def subcmd(args):
            return args.aa, args.bb, args.cc, args.dd

        self.parser = pyutil.CommandParser('prog')
        self.parser.add_argument('-v', '--verbose', action='count')

    def test_count(self):
        args = self.parser.parse_args(['-v', '-vv'])
        self.assertEqual(args.verbose, 3)


class TestCommandParserTree(unittest.TestCase):
    def setUp(self):
        def configure(args):
            return 'configure', args.aa, args.bb, args.cc, args.dd

        def build(args):
            return 'build', args.aa, args.bb, args.cc, args.dd

        def configure_db(args):
            return 'configure_db', args.aa, args.bb, args.cc, args.dd

        self.parser = pyutil.CommandParser('prog', help='the main program')
        self.parser.add_argument('-a', '--aa', action='count', help='count a')

        devparser = self.parser.add_command(
            'development', help='development operations'
        )
        devparser.add_argument('-b', '--bb', action='count', help='count b')
        devparser.add_argument('-c', '--cc', action='count', help='count c')

        confparser = devparser.add_command(
            'configure', func=configure, help='configure the build'
        )
        confparser.add_argument('-d', '--dd', action='count', help='count d')

        buildparser = devparser.add_command(
            'build', func=build, help='build the product'
        )
        buildparser.add_argument('-d', '--dd', action='count', help='count d')
        buildparser.add_argument('-e', '--ee', action='count', help='count e')

        dbparser = self.parser.add_command(
            'database', help='the configuration database'
        )
        dbparser.add_argument('-b', '--bb', action='count', help='count b')
        dbparser.add_argument('-c', '--cc', action='count', help='count c')

        confdbparser = dbparser.add_command(
            'configure', func=configure_db, help='configure the database'
        )
        confdbparser.add_argument('-d', '--dd', action='count', help='count d')

    def test_groups(self):
        pass


class TestArgsAfterCommands(unittest.TestCase):
    def setUp(self):
        def init(args):
            print('init')
            print(args)

        def push(args):
            print('push')
            print(args)

        parser = pyutil.CommandParser('test')

        parser.add_argument('-a', '--aaa', action='count', help='aaa help')
        parser.add_argument('-b', '--bbb', action='store_true', help='bbb help')
        parser.add_argument(
            '--config', default='~/.config/test', help='configuration file'
        )

        initparser = parser.add_command('init', func=init, help='initialize something')
        initparser.add_argument('-c', '--ccc', action='count', help='init ccc help')
        initparser.add_argument('-d', '--ddd', action='store_true', help='ddd help')
        initparser.add_argument('-e', '--eee', default='-', help='eee help')
        initparser.add_argument('input', help='input file or - for stdin')

        pushparser = parser.add_command('push', func=push, help='push something')
        pushparser.add_argument('-c', '--ccc', action='count', help='push ccc help')
        pushparser.add_argument('-f', '--fff', action='store_true', help='fff help')
        pushparser.add_argument('-e', '--egg', default='-', help='egg help')
        pushparser.add_argument('input', help='input file or - for stdin')

        def error(msg):
            raise RuntimeError(msg)

        def exit():
            raise RuntimeError()

        parser._build_parser(None, None)
        parser.parser.error = error
        parser.parser.exit = exit
        for cmd, cmdparser in parser.commands.items():
            cmdparser.parser.error = error
            cmdparser.parser.exit = exit

        self.parser = parser

    def test_no_posargs_and_subcmds(self):
        with self.assertRaises(ValueError):
            self.parser.add_argument('spec1', help='global spec 1')
        with self.assertRaises(ValueError):
            self.parser.commands['init'].add_command('init-subcmd', lambda args: None)

    def test_no_args_help(self):
        args = self.parser.parse_args([])
        with io.StringIO() as buf:
            with contextlib.redirect_stdout(buf):
                args = self.parser.parse_args([])
                args.func(args)
            ptrn = re.compile('usage: test.*{help,init,push}', flags=re.M | re.S)
            self.assertRegex(buf.getvalue(), ptrn)

    def assert_help(self, args, pattern):
        with io.StringIO() as buf:
            with contextlib.redirect_stdout(buf):
                with self.assertRaises(RuntimeError):
                    args = self.parser.parse_args(args)
                    args.func(args)
            self.assertRegex(buf.getvalue(), re.compile(pattern, flags=re.M | re.S))

    def test_help_flags(self):
        self.assert_help(['-h'], 'usage: test.*{help,init,push}')
        self.assert_help(['--help'], 'usage: test.*{help,init,push}')

    def test_subcmd_help_flags(self):
        self.assert_help(['init', '-h'], r'usage: test.*\s+init\s')
        self.assert_help(['init', '--help'], r'usage: test.*\s+init\s')

    def test_help_cmd(self):
        self.assert_help(['help'], r'usage: ')


if __name__ == '__main__':
    import test

    test.main()
