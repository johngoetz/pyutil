import os
import pathlib
import shutil
import tempfile
import unittest

from unittest.mock import patch

import pyutil


class TestFileOperations(unittest.TestCase):
    def test_directory_size(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            dtmp = pathlib.Path(tmpdir)
            (dtmp / 'one').write_text('one two three')
            (dtmp / 'two').write_text('blah')
            (dtmp / 'three').mkdir(parents=True)
            (dtmp / 'three/four').write_text('abc')

            result = pyutil.directory_size(dtmp)
            self.assertEqual(result, 20)

    def test_synctree(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            dtmp = pathlib.Path(tmpdir)

            (dtmp / 'a').mkdir()
            (dtmp / 'a/same.txt').write_text('same')
            (dtmp / 'a/different.txt').write_text('different')
            (dtmp / 'a/missing.txt').write_text('missing')
            (dtmp / 'a/readonly.txt').write_text('readonly')
            (dtmp / 'a/subdir').mkdir()
            (dtmp / 'a/subdir/same.txt').write_text('same')
            (dtmp / 'a/subdir/different.txt').write_text('different')
            (dtmp / 'a/subdir/missing.txt').write_text('missing')
            (dtmp / 'a/missingdir').mkdir()
            (dtmp / 'a/missingdir/missing.txt').write_text('missing')

            (dtmp / 'b').mkdir()
            (dtmp / 'b/same.txt').write_text('same')
            (dtmp / 'b/different.txt').write_text('*********')
            (dtmp / 'b/new.txt').write_text('new')
            (dtmp / 'b/subdir').mkdir()
            (dtmp / 'b/subdir/same.txt').write_text('same')
            (dtmp / 'b/subdir/different.txt').write_text('*********')
            (dtmp / 'b/subdir/new.txt').write_text('new')
            (dtmp / 'b/subdir/new-readonly.txt').write_text('new-readonly')
            (dtmp / 'b/subdir/new-readonly-dir').mkdir(parents=True)
            (dtmp / 'b/newdir').mkdir()
            (dtmp / 'b/newdir/new.txt').write_text('new')

            os.chmod((dtmp / 'a/readonly.txt'), 0o400)
            os.chmod((dtmp / 'b/subdir/new-readonly.txt'), 0o400)
            os.chmod((dtmp / 'b/subdir/new-readonly-dir'), 0o400)

            pyutil.synctree(dtmp / 'a', dtmp / 'b')

            self.assertEqual((dtmp / 'b/same.txt').read_text(), 'same')
            self.assertEqual((dtmp / 'b/different.txt').read_text(), 'different')
            self.assertEqual((dtmp / 'b/missing.txt').read_text(), 'missing')
            self.assertEqual((dtmp / 'b/readonly.txt').read_text(), 'readonly')
            self.assertFalse((dtmp / 'b/new.txt').exists())
            self.assertEqual((dtmp / 'b/subdir/same.txt').read_text(), 'same')
            self.assertEqual((dtmp / 'b/subdir/different.txt').read_text(), 'different')
            self.assertEqual((dtmp / 'b/subdir/missing.txt').read_text(), 'missing')
            self.assertFalse((dtmp / 'b/subdir/new.txt').exists())
            self.assertFalse((dtmp / 'b/subdir/new-readonly.txt').exists())
            self.assertFalse((dtmp / 'b/subdir/new-readonly-dir').exists())
            self.assertEqual((dtmp / 'b/missingdir/missing.txt').read_text(), 'missing')
            self.assertFalse((dtmp / 'b/newdir').exists())

    def test_synctree_missing_dest(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            dtmp = pathlib.Path(tmpdir)

            (dtmp / 'a').mkdir()
            (dtmp / 'a/test.txt').write_text('test')

            self.assertFalse((dtmp / 'b').exists())
            pyutil.synctree(dtmp / 'a', dtmp / 'b')

            self.assertEqual((dtmp / 'b/test.txt').read_text(), 'test')

    def test_rsync(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            dtmp = pathlib.Path(tmpdir)

            (dtmp / 'a').mkdir()
            (dtmp / 'a/test.txt').write_text('test')
            (dtmp / 'a/subdir').mkdir()
            (dtmp / 'a/subdir/test.txt').write_text('test')

            self.assertFalse((dtmp / 'b').exists())
            pyutil.rsync(dtmp / 'a', dtmp / 'b')

            self.assertEqual((dtmp / 'b/a/test.txt').read_text(), 'test')
            self.assertEqual((dtmp / 'b/a/subdir/test.txt').read_text(), 'test')
            shutil.rmtree(dtmp / 'b')

            pyutil.rsync([dtmp / 'a/test.txt', dtmp / 'a/subdir'], dtmp / 'b')
            self.assertEqual((dtmp / 'b/test.txt').read_text(), 'test')
            self.assertEqual((dtmp / 'b/subdir/test.txt').read_text(), 'test')

    def test_closed_temp_file(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            dtmp = pathlib.Path(tmpdir)
            with pyutil.ClosedTemporaryFile(dir=dtmp) as ftmp:
                self.assertTrue(ftmp.is_file())
                ftmp.write_text('testing')
                src = ftmp.read_text()
                self.assertEqual('testing', src)
            self.assertFalse(ftmp.is_file())

            with pyutil.ClosedTemporaryFile(dir=dtmp, delete=False) as ftmp:
                try:
                    ftmp.write_text('test1')
                    self.assertEqual(ftmp.read_text(), 'test1')
                finally:
                    ftmp.unlink()

    def test_existing_dir(self):
        here = pathlib.Path(__file__).parent
        d = pyutil.ExistingDir(str(here))
        self.assertEqual(here, d)

        with self.assertLogs(level='ERROR') as l:
            with self.assertRaises(FileNotFoundError):
                _ = pyutil.ExistingDir(here / 'non-existant-directory')
            self.assertEqual(len(l.output), 1)

        with self.assertLogs(level='ERROR') as l:
            with self.assertRaises(FileNotFoundError):
                _ = pyutil.ExistingDir(__file__)
            self.assertEqual(len(l.output), 1)

    def test_secret(self):
        self.assertEqual('test', pyutil.Secret('test'))

        with tempfile.TemporaryDirectory() as dtmp:
            f = pathlib.Path(dtmp) / 'secret'
            f.write_text('test')

            with self.assertRaises(PermissionError):
                _ = pyutil.Secret(f)

            f.chmod(0o600)
            self.assertEqual('test', pyutil.Secret(f))

    def test_tee(self):
        with pyutil.ClosedTemporaryFile() as ftmp:
            with tempfile.TemporaryFile(mode='w+t') as tmppipe:
                with pyutil.tee(ftmp, pipe=tmppipe) as f:
                    f.write('testing\n')
                self.assertEqual(ftmp.read_text(), 'testing\n')

    def test_tee_skip(self):
        with pyutil.ClosedTemporaryFile() as ftmp:
            with tempfile.TemporaryFile(mode='w+t') as tmppipe:
                with pyutil.tee(ftmp, pipe=tmppipe, skip=2) as f:
                    for i in range(3):
                        f.write(f'test {i}\n')
                self.assertEqual(ftmp.read_text(), 'test 0\ntest 1\ntest 2\n')
                tmppipe.flush()
                tmppipe.seek(0)
                out = tmppipe.read()
                self.assertTrue(
                    out.startswith(
                        '[showing 1 out of every 2 lines of output]\ntest 1\n'
                    )
                )
                self.assertIn('> tail -20 ', out)
                self.assertTrue(out.endswith('test 0\ntest 1\ntest 2\n'))

    def test_tee_skip_without_newline(self):
        with pyutil.ClosedTemporaryFile() as ftmp:
            with tempfile.TemporaryFile(mode='w+t') as tmppipe:
                with pyutil.tee(ftmp, pipe=tmppipe, skip=2) as f:
                    for i in range(3):
                        f.write(f'test {i}\n')
                    f.write('test 4')  # without newline (for testing)
                self.assertEqual(ftmp.read_text(), 'test 0\ntest 1\ntest 2\ntest 4')
                tmppipe.flush()
                tmppipe.seek(0)
                out = tmppipe.read()
                self.assertTrue(
                    out.startswith(
                        '[showing 1 out of every 2 lines of output]\ntest 1\n'
                    )
                )
                self.assertIn('> tail -20 ', out)
                self.assertTrue(out.endswith('test 0\ntest 1\ntest 2\ntest 4\n'))


if __name__ == '__main__':
    import test

    test.main()
