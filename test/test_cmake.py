import unittest

import pyutil


class TestCMake(unittest.TestCase):
    def test_cmake_bool(self):
        self.assertEqual(pyutil.cmake_bool(True), 'TRUE')
        self.assertEqual(pyutil.cmake_bool(1), 'TRUE')
        self.assertEqual(pyutil.cmake_bool(2), 'TRUE')
        self.assertEqual(pyutil.cmake_bool('1'), 'TRUE')
        self.assertEqual(pyutil.cmake_bool('true'), 'TRUE')
        self.assertEqual(pyutil.cmake_bool('yes'), 'TRUE')
        self.assertEqual(pyutil.cmake_bool('on'), 'TRUE')
        self.assertEqual(pyutil.cmake_bool('True'), 'TRUE')
        self.assertEqual(pyutil.cmake_bool('YES'), 'TRUE')
        self.assertEqual(pyutil.cmake_bool('ON'), 'TRUE')

        self.assertEqual(pyutil.cmake_bool(False), 'FALSE')
        self.assertEqual(pyutil.cmake_bool(0), 'FALSE')
        self.assertEqual(pyutil.cmake_bool(''), 'FALSE')
        self.assertEqual(pyutil.cmake_bool('0'), 'FALSE')
        self.assertEqual(pyutil.cmake_bool('false'), 'FALSE')
        self.assertEqual(pyutil.cmake_bool('no'), 'FALSE')
        self.assertEqual(pyutil.cmake_bool('off'), 'FALSE')
        self.assertEqual(pyutil.cmake_bool('False'), 'FALSE')
        self.assertEqual(pyutil.cmake_bool('NO'), 'FALSE')
        self.assertEqual(pyutil.cmake_bool('OFF'), 'FALSE')

        self.assertEqual(pyutil.cmake_bool('blah blah blah'), 'FALSE')

    def test_as_bool(self):
        self.assertTrue(pyutil.as_bool(True))
        self.assertTrue(pyutil.as_bool(1))
        self.assertTrue(pyutil.as_bool(2))
        self.assertTrue(pyutil.as_bool('1'))
        self.assertTrue(pyutil.as_bool('true'))
        self.assertTrue(pyutil.as_bool('yes'))
        self.assertTrue(pyutil.as_bool('on'))
        self.assertTrue(pyutil.as_bool('True'))
        self.assertTrue(pyutil.as_bool('YES'))
        self.assertTrue(pyutil.as_bool('ON'))

        self.assertFalse(pyutil.as_bool(False))
        self.assertFalse(pyutil.as_bool(0))
        self.assertFalse(pyutil.as_bool('0'))
        self.assertFalse(pyutil.as_bool('false'))
        self.assertFalse(pyutil.as_bool('no'))
        self.assertFalse(pyutil.as_bool('off'))
        self.assertFalse(pyutil.as_bool('False'))
        self.assertFalse(pyutil.as_bool('NO'))
        self.assertFalse(pyutil.as_bool('OFF'))

        self.assertFalse(pyutil.as_bool('blah blah blah'))


if __name__ == '__main__':
    import test

    test.main()
