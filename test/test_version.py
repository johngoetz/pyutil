import unittest

import pyutil


class TestVersion(unittest.TestCase):
    def test_version(self):
        self.assertRegex(pyutil.__version__, r'\d+\.\d+\.\d+')


if __name__ == '__main__':
    import test

    test.main()
