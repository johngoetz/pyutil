import os
import pathlib
import unittest

import pyutil


here = pathlib.Path(__file__).parent


class TestGitOps(unittest.TestCase):
    def test_is_git_repo(self):
        self.assertTrue(pyutil.is_git_repo(here))
        self.assertFalse(pyutil.is_git_repo(pathlib.Path('/')))

    def test_git_topdir(self):
        self.assertEqual(pyutil.git_topdir(here, superproject=False), (here / '..').resolve())


class TestGitRepository(unittest.TestCase):
    def test_commit(self):
        repo = pyutil.GitRepository(here, superproject=False)
        self.assertRegex(repo.commit, r'\w+')
        self.assertRegex(repo.branch, r'\w+')
        self.assertRegex(repo.remote, r'\w+')
        self.assertRegex(repo.remote_url, r'(git@|http).*')
        self.assertRegex(repo.server_address, r'\w+\.\w+')
        self.assertRegex(repo.project_path, r'\w+/pyutil')
        self.assertRegex(repo.namespace, r'\w+')
        self.assertRegex(repo.project, 'pyutil')
        self.assertTrue(repo.has_local_changes or True)
        self.assertGreaterEqual(repo.commit_count_to_branch('main'), 0)


if __name__ == '__main__':
    import test

    test.main()
