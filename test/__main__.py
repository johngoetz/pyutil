import sys

from .test_appdirs import *
from .test_cmake import *
from .test_cmdparser import *
from .test_env import *
from .test_file_operations import *
from .test_gitlab import *
from .test_subproc import *
from .test_repository import  *
from .test_version import *


if __name__ == '__main__':
    import test

    test.main()
