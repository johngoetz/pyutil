import os
import pathlib
import sys
import unittest
from unittest.mock import Mock, patch

from pyutil.appdirs import *


class TestXDG(unittest.TestCase):
    def test_cache_dir(self):
        with patch.dict(os.environ, {'XDG_CACHE_HOME': 'test'}):
            self.assertEqual(xdg.cache_dir(), pathlib.Path('test'))

    def test_config_dir(self):
        with patch.dict(os.environ, {'XDG_CONFIG_HOME': 'test'}):
            self.assertEqual(xdg.config_dir(), pathlib.Path('test'))

    def test_data_dir(self):
        with patch.dict(os.environ, {'XDG_DATA_HOME': 'test'}):
            self.assertEqual(xdg.data_dir(), pathlib.Path('test'))

    def test_runtime_dir(self):
        with patch.dict(os.environ, {'XDG_RUNTIME_DIR': 'test'}):
            self.assertEqual(xdg.runtime_dir(), pathlib.Path('test'))


class TestUser(unittest.TestCase):
    def test_home_dir(self):
        self.assertEqual(User.home_dir(), pathlib.Path.home())


class TestLinuxUser(unittest.TestCase):
    def test_home_dir(self):
        self.assertEqual(LinuxUser.home_dir(), User.home_dir())

    def test_cache_dir(self):
        with patch.dict(os.environ, {'XDG_CACHE_HOME': 'test'}):
            self.assertEqual(LinuxUser.cache_dir(), xdg.cache_dir())

        with patch.dict(os.environ, {}):
            self.assertEqual(LinuxUser.cache_dir(), User.home_dir() / '.cache')

    def test_config_dir(self):
        with patch.dict(os.environ, {'XDG_CONFIG_HOME': 'test'}):
            self.assertEqual(LinuxUser.config_dir(), xdg.config_dir())

        with patch.dict(os.environ, {}):
            self.assertEqual(LinuxUser.config_dir(), User.home_dir() / '.config')

    def test_data_dir(self):
        with patch.dict(os.environ, {'XDG_DATA_HOME': 'test'}):
            self.assertEqual(LinuxUser.data_dir(), xdg.data_dir())

        with patch.dict(os.environ, {}):
            self.assertEqual(LinuxUser.data_dir(), User.home_dir() / '.local/share')

    def test_runtime_dir(self):
        with patch.dict(os.environ, {'XDG_RUNTIME_HOME': 'test'}):
            self.assertEqual(LinuxUser.runtime_dir(), xdg.runtime_dir())

        with patch.dict(os.environ, {}):
            self.assertEqual(
                LinuxUser.runtime_dir(),
                pathlib.Path(f'/run/user/{os.getuid()}'),
            )


class TestLinuxApp(unittest.TestCase):
    def test_cache_dir(self):
        self.assertEqual(LinuxApp.cache_dir('App'), LinuxUser.cache_dir() / 'App')
        self.assertEqual(
            LinuxApp.cache_dir('App TEST', 'company', 'com'),
            LinuxUser.cache_dir() / 'AppTEST',
        )

    def test_config_dir(self):
        self.assertEqual(LinuxApp.config_dir('App'), LinuxUser.config_dir() / 'App')
        self.assertEqual(
            LinuxApp.config_dir('App TEST', 'company', 'com'),
            LinuxUser.config_dir() / 'AppTEST',
        )

    def test_data_dir(self):
        self.assertEqual(LinuxApp.data_dir('App'), LinuxUser.data_dir() / 'App')
        self.assertEqual(
            LinuxApp.data_dir('App TEST', 'company', 'com'),
            LinuxUser.data_dir() / 'AppTEST',
        )
        self.assertEqual(
            LinuxApp.local_data_dir('App', 'company'),
            LinuxApp.data_dir('App', 'company'),
        )
        self.assertEqual(
            LinuxApp.roaming_data_dir('App', 'company'),
            LinuxApp.data_dir('App', 'company'),
        )

    def test_runtime_dir(self):
        self.assertEqual(LinuxApp.runtime_dir('App'), LinuxUser.runtime_dir() / 'App')


class TestDarwinUser(unittest.TestCase):
    def test_home_dir(self):
        self.assertEqual(DarwinUser.home_dir(), User.home_dir())

    def test_cache_dir(self):
        with patch.dict(os.environ, {'XDG_CACHE_HOME': 'test'}):
            self.assertEqual(DarwinUser.cache_dir(), xdg.cache_dir())

        with patch.dict(os.environ, {}):
            self.assertEqual(DarwinUser.cache_dir(), User.home_dir() / 'Library/Caches')

    def test_config_dir(self):
        with patch.dict(os.environ, {'XDG_CONFIG_HOME': 'test'}):
            self.assertEqual(DarwinUser.config_dir(), xdg.config_dir())

        with patch.dict(os.environ, {}):
            self.assertEqual(
                DarwinUser.config_dir(),
                User.home_dir() / 'Library/Preferences',
            )

    def test_data_dir(self):
        with patch.dict(os.environ, {'XDG_DATA_HOME': 'test'}):
            self.assertEqual(DarwinUser.data_dir(), xdg.data_dir())

        with patch.dict(os.environ, {}):
            self.assertEqual(
                DarwinUser.data_dir(),
                User.home_dir() / 'Library/Application Support',
            )


class TestDarwinApp(unittest.TestCase):
    def test_cache_dir(self):
        self.assertEqual(DarwinApp.cache_dir('app'), DarwinUser.cache_dir() / 'app')

        self.assertEqual(DarwinApp.cache_dir('App'), DarwinUser.cache_dir() / 'App')

        self.assertEqual(
            DarwinApp.cache_dir('app TEST', 'company', 'com'),
            DarwinUser.cache_dir() / 'com.company.app-TEST',
        )

        self.assertEqual(
            DarwinApp.cache_dir('app TEST', 'company'),
            DarwinUser.cache_dir() / 'company.app-TEST',
        )

    def test_config_dir(self):
        self.assertEqual(
            DarwinApp.config_dir('app'),
            DarwinUser.config_dir().joinpath('app'),
        )

        self.assertEqual(
            DarwinApp.config_dir('App'),
            DarwinUser.config_dir().joinpath('App'),
        )

        self.assertEqual(
            DarwinApp.config_dir('app TEST', 'company', 'com'),
            DarwinUser.config_dir() / 'com.company.app-TEST',
        )

        self.assertEqual(
            DarwinApp.config_dir('app TEST', 'company'),
            DarwinUser.config_dir().joinpath('company.app-TEST'),
        )
        self.assertEqual(
            DarwinApp.runtime_dir('app TEST'), DarwinApp.cache_dir('app TEST')
        )

    def test_data_dir(self):
        self.assertEqual(
            DarwinApp.data_dir('app'), DarwinUser.data_dir().joinpath('app')
        )

        self.assertEqual(
            DarwinApp.data_dir('App'), DarwinUser.data_dir().joinpath('App')
        )

        self.assertEqual(
            DarwinApp.data_dir('app TEST', 'company', 'com'),
            DarwinUser.data_dir().joinpath('com.company.app-TEST'),
        )

        self.assertEqual(
            DarwinApp.data_dir('app TEST', 'company'),
            DarwinUser.data_dir().joinpath('company.app-TEST'),
        )
        self.assertEqual(
            DarwinApp.local_data_dir('App', 'company'),
            DarwinApp.data_dir('App', 'company'),
        )
        self.assertEqual(
            DarwinApp.roaming_data_dir('App', 'company'),
            DarwinApp.data_dir('App', 'company'),
        )


class TestWindowsUser(unittest.TestCase):
    def test_home_dir(self):
        with patch.dict(os.environ, {'USERPROFILE': 'test'}):
            self.assertEqual(WindowsUser.home_dir(), pathlib.Path('test'))

        with patch.dict(os.environ, {}):
            self.assertEqual(WindowsUser.home_dir(), User.home_dir())

    def test_local_data_dir(self):
        with patch.dict(os.environ, {'LOCALAPPDATA': 'test'}):
            self.assertEqual(WindowsUser.local_data_dir(), pathlib.Path('test'))

        with patch.dict(os.environ, {}):
            self.assertEqual(
                WindowsUser.local_data_dir(),
                WindowsUser.home_dir().joinpath('AppData', 'Local'),
            )

    def test_roaming_data_dir(self):
        with patch.dict(os.environ, {'APPDATA': 'test'}):
            self.assertEqual(WindowsUser.roaming_data_dir(), pathlib.Path('test'))

        with patch.dict(os.environ, {}):
            self.assertEqual(
                WindowsUser.roaming_data_dir(),
                WindowsUser.home_dir().joinpath('AppData', 'Roaming'),
            )


class TestWindowsApp(unittest.TestCase):
    def test_cache_dir(self):
        self.assertEqual(
            WindowsApp.cache_dir('app'),
            WindowsUser.local_data_dir().joinpath('app', 'Cache'),
        )

        self.assertEqual(
            WindowsApp.cache_dir('app TEST', 'company', 'com'),
            WindowsUser.local_data_dir().joinpath('company', 'app TEST', 'Cache'),
        )

    def test_config_dir(self):
        self.assertEqual(
            WindowsApp.config_dir('app'),
            WindowsUser.roaming_data_dir().joinpath('app', 'Config'),
        )

        self.assertEqual(
            WindowsApp.config_dir('app TEST', 'company', 'com'),
            WindowsUser.roaming_data_dir().joinpath('company', 'app TEST', 'Config'),
        )
        self.assertEqual(
            WindowsApp.runtime_dir('app TEST'), WindowsApp.cache_dir('app TEST')
        )

    def test_data_dir(self):
        self.assertEqual(
            WindowsApp.data_dir('app'),
            WindowsUser.local_data_dir().joinpath('app', 'Data'),
        )

        self.assertEqual(
            WindowsApp.data_dir('app TEST', 'company', 'com'),
            WindowsUser.local_data_dir().joinpath('company', 'app TEST', 'Data'),
        )

    def test_local_data_dir(self):
        self.assertEqual(
            WindowsApp.local_data_dir('app'),
            WindowsUser.local_data_dir().joinpath('app', 'Data'),
        )

        self.assertEqual(
            WindowsApp.local_data_dir('app TEST', 'company'),
            WindowsUser.local_data_dir().joinpath('company', 'app TEST', 'Data'),
        )

    def test_roaming_data_dir(self):
        self.assertEqual(
            WindowsApp.roaming_data_dir('app'),
            WindowsUser.roaming_data_dir().joinpath('app', 'Data'),
        )

        self.assertEqual(
            WindowsApp.roaming_data_dir('app TEST', 'company'),
            WindowsUser.roaming_data_dir().joinpath('company', 'app TEST', 'Data'),
        )


if __name__ == '__main__':
    import test

    test.main()
