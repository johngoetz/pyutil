import os
import pathlib
import unittest

import gitlab

import pyutil

"""
class TestBranchList(unittest.TestCase):
    def setUp(self):
        try:
            private_token = pyutil.Secret('~/.gitlab_private_token')
            if private_token == '~/.gitlab_private_token':
                raise OSError(f'missing ~/.gitlab_private_token')
        except Exception as e:
            raise unittest.SkipTest(str(e))
        self.gl = gitlab.Gitlab('https://git.tecplot.com', private_token=private_token)

    def test_this_repo(self):
        project = pathlib.Path(__file__).parent
        branches, merge_request = pyutil.gitlab.branch_list(self.gl, project)
        print('\n', project, branches, merge_request)

    def test_base_project(self):
        project = 'products/tecplot'
        branches, merge_request = pyutil.gitlab.branch_list(self.gl, project)
        print('\n', project, branches, merge_request)

    def test_fork(self):
        project = 'john/tecplot'
        branches, merge_request = pyutil.gitlab.branch_list(self.gl, project)
        print('\n', project, branches, merge_request)

    def test_documentation(self):
        project = pathlib.Path(os.getcwd())
        source_project = 'products/documentation'
        branches, merge_request = pyutil.gitlab.branch_list(
            self.gl, project, source_project
        )
        print('\n', project, source_project, branches, merge_request)

    def test_pytecplot(self):
        project = pathlib.Path(os.getcwd())
        source_project = 'products/pytecplot'
        branches, merge_request = pyutil.gitlab.branch_list(
            self.gl, project, source_project
        )
        print('\n', project, source_project, branches, merge_request)
"""

if __name__ == '__main__':
    import test

    test.main()
