import asyncio
import contextlib
import logging
import logging.handlers
import io
import queue
import subprocess
import sys
import tempfile
import unittest

import pyutil


class TestRunSubprocess(unittest.TestCase):
    def test_sys_output(self):
        msg = 'testing\n1\n2\n3\ndone.'
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            rc = pyutil.run_subprocess('echo', '-n', msg, stdout=sys.stdout)
            self.assertEqual(rc, 0)
        self.assertEqual(msg, f.getvalue())

    def test_file_path_output(self):
        msg = 'testing\n1\n2\n3\ndone.'
        with pyutil.ClosedTemporaryFile() as ftmp:
            pyutil.run_subprocess('echo', '-n', msg, stdout=ftmp)
            self.assertEqual(msg, ftmp.read_text())

    def test_file_output(self):
        msg = 'testing\n1\n2\n3\ndone.'
        with pyutil.ClosedTemporaryFile() as ftmp:
            pyutil.run_subprocess('echo', '-n', msg, stdout=str(ftmp))
            self.assertEqual(msg, ftmp.read_text())

    def test_bytes_fileobj_output(self):
        msg = 'testing\n1\n2\n3\ndone.'
        with tempfile.TemporaryFile() as ftmp:
            pyutil.run_subprocess('echo', '-n', msg, stdout=ftmp, text=False)
            ftmp.seek(0)
            out = ftmp.read()
            self.assertEqual(msg, out.decode())

    def test_text_fileobj_output(self):
        msg = 'testing\n1\n2\n3\ndone.'
        with tempfile.TemporaryFile(mode='t+w') as ftmp:
            pyutil.run_subprocess('echo', '-n', msg, stdout=ftmp)
            ftmp.flush()
            ftmp.seek(0)
            out = ftmp.read()
            self.assertEqual(msg, out)

    def test_logged_output(self):
        log = logging.getLogger(__name__)
        q = queue.SimpleQueue()
        handler = logging.handlers.QueueHandler(q)
        log.addHandler(handler)
        msg = 'testing\n1\n2\n3\ndone.'
        try:
            pyutil.run_subprocess('echo', msg, stdout=log.warning, strip_text=True)
        finally:
            log.removeHandler(handler)
        for line in msg.split('\n'):
            self.assertEqual(q.get_nowait().getMessage(), line)

    def test_polling(self):
        log = logging.getLogger(__name__)
        q = queue.SimpleQueue()
        handler = logging.handlers.QueueHandler(q)
        log.addHandler(handler)
        msg = 'testing\n1\n2\n3\ndone.'
        try:

            async def poll(proc, limit=2**16):
                while proc.returncode is None:
                    try:
                        data = await proc.stdout.readuntil(b'\n')
                    except asyncio.LimitOverrunError:
                        data = await proc.stdout.read(limit)
                    except asyncio.IncompleteReadError as e:
                        log.warning(e.partial.decode().strip())
                        break
                    if data is None:
                        break
                    log.warning(data.decode().strip())

            pyutil.run_subprocess(
                'echo', msg, stdout=subprocess.PIPE, strip_text=True, poll=poll
            )

        finally:
            log.removeHandler(handler)
        for line in msg.split('\n'):
            self.assertEqual(q.get_nowait().getMessage(), line)


if __name__ == '__main__':
    import test

    test.main()
